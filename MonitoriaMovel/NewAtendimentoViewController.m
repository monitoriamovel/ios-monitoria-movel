//
//  NewAtendimentoViewController.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 07/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "NewAtendimentoViewController.h"
#import "JVFloatLabeledTextField.h"
#import "UIColor+MM.h"
#import "UIFont+MM.h"
#import "DateFormatter.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>

@interface NewAtendimentoViewController ()
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIDatePicker *datePicker;

@property (nonatomic, strong) JVFloatLabeledTextField *dateField;
@property (nonatomic, strong) JVFloatLabeledTextField *monitorSelecionadoField;

@property (nonatomic, strong) UIButton *agendarButton;
@property (nonatomic, strong) User *monitorSelecionado;

@property (nonatomic, strong) Atendimento *atendimentoSelecionado;

@end

@implementation NewAtendimentoViewController

- (instancetype)initWithMonitorSelecionado:(User *) monitor
{
    self = [super init];
    if (self) {
        _monitorSelecionado = monitor;
    }
    return self;
}


- (instancetype)initWithAtendimentoSelecionado:(Atendimento *) atendimento
{
    self = [super init];
    if (self) {
        _atendimentoSelecionado = atendimento;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Novo agendamento";
    
    UIImage *imgBack = [[UIImage imageNamed:@"ic_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:imgBack
                                                                               style:UIBarButtonItemStylePlain
                                                                              target:self action:@selector(dismiss)]];


    [self.view setBackgroundColor:[UIColor colorFromHexString:@"f2f2f2"]];
    
    
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:_scrollView];
    
    float width = _scrollView.frame.size.width;
    
    int y = 20;
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [_datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    [_datePicker setMinimumDate:[NSDate date]];
    [_datePicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];

    
    _dateField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [self configureTextField:_dateField withString:@"Horário desejado"];
    [_dateField addTarget:_dateField action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [_scrollView addSubview:_dateField];
    _dateField.keepBaseline = YES;
    _dateField.inputView = _datePicker;

    
    y += 60;
    
    if(self.monitorSelecionado) {
        _monitorSelecionadoField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
        [self configureTextField:_monitorSelecionadoField withString:@"Monitor selecionado"];
        [_scrollView addSubview:_monitorSelecionadoField];
        _monitorSelecionadoField.keepBaseline = YES;
        [_monitorSelecionadoField setEnabled:NO];

        [_monitorSelecionadoField setText:_monitorSelecionado.nome];
        
        
        y += 60;
    }
    
    
    _agendarButton = [[UIButton alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [_agendarButton addTarget:self action:@selector(touchSolicitaAgendamento) forControlEvents:UIControlEventTouchUpInside];
    [_agendarButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_agendarButton setBackgroundColor:[UIColor furbBlue]];
    [_agendarButton.layer setCornerRadius:2.0f];
    [_scrollView addSubview:_agendarButton];
    
    if(self.monitorSelecionado) {
        [_agendarButton setTitle:@"Solicitar agendamento" forState:UIControlStateNormal];

    } else {
        [_agendarButton setTitle:@"Agendar esse horário" forState:UIControlStateNormal];
    }
    
    
    [_scrollView setContentSize:CGSizeMake(width, y+60)];
    
}

#pragma mark - Picker value change

- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker {
    _dateField.text = [[DateFormatter sharedFormatter] diaHoraFormatter:datePicker.date];
}

#pragma mark - Button methods

- (void) touchSolicitaAgendamento {
    //Cria no parse uma solicitação
    
    [self.view endEditing:YES];
    
    
    
    if(_dateField.text.length == 0) {
        [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Por favor, selecione uma data." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];

        return;
    }
    [SVProgressHUD showWithStatus:@"Solicitando..."];

    
    if (self.atendimentoSelecionado) {
        //Editando
        
        PFQuery *query = [PFQuery queryWithClassName:@"atendimento"];
        
        [query getObjectInBackgroundWithId:self.atendimentoSelecionado.myself.objectId block:^(PFObject * _Nullable object, NSError * _Nullable error) {
            [SVProgressHUD dismiss];
            
            if(!error) {
                
                object[@"horario_marcado"] = _datePicker.date;
                object[@"status"] = @(0);
                
                [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    
                    [SVProgressHUD showSuccessWithStatus:@"Solicitado com sucesso!" ];

                    [self.navigationController popViewControllerAnimated:YES];
                }];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Ocorreu um erro.\nTente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            }
        }];
        
        
        
        
    } else {
    //Criando um novo atendimento
        PFObject *atend = [PFObject objectWithClassName:@"atendimento"];
        atend[@"horario_marcado"] = _datePicker.date;
        atend[@"encerrado"] = @(0);
        atend[@"horario_aceito"] = @(0);
        atend[@"status"] = @(0);
        
        atend[@"usuario"] = [[AppDelegate sharedDelegate].loggedUser getParseObject];
        atend[@"monitor"] = [self.monitorSelecionado getParseObject];
        
        
        //Remover isso (inverter):
//        atend[@"monitor"] = [[AppDelegate sharedDelegate].loggedUser getParseObject];
//        atend[@"usuario"] = [self.monitorSelecionado getParseObject];
        //fim teste
        atend[@"avaliacao"] = @(-1);
        
        [atend saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [SVProgressHUD dismiss];
            if (succeeded) {
                [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"O monitor recebeu sua solicitação. Aguarde uma resposta dele!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                [self dismiss];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Ocorreu um erro, tente novamente!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            }
            
        }];
    }
}

- (void)dismiss {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIView refactor methods

- (void) configureTextField:(JVFloatLabeledTextField *)textField withString:(NSString *) str{
    
    //Field settings
    textField.font = [UIFont avenirRegularWithSize:16.0f];
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str
                                                                      attributes:@{NSForegroundColorAttributeName: [UIColor furbBlue]}];
    textField.floatingLabelFont = [UIFont avenirBoldWithSize:11.0f];
    textField.floatingLabelTextColor = [UIColor furbBlue];
    textField.floatingLabelActiveTextColor = [UIColor furbYellow];
    [textField setTextColor:[UIColor furbBlue]];
    
    
    //Keyboard settings
    [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [textField setReturnKeyType:UIReturnKeyDone];
}

@end
