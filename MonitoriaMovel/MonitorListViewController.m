//
//  MonitorListViewController.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 09/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//
#import "AppDelegate.h"
#import "ProfileViewController.h"
#import "MonitorListViewController.h"
#import "MonitorTableViewCell.h"
#import "UIColor+MM.h"
#import "UIFont+MM.h"
#import "User.h"
#import <Parse/Parse.h>

@interface MonitorListViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *monitorsArray;
@property (nonatomic, strong) NSString *filterString;
@end

@implementation MonitorListViewController
static NSString *kMonitorCell = @"kMonitorCell";


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _monitorsArray = [NSMutableArray array];

//        for (int i = 0; i < 20; i++) {
//            [_monitorsArray addObject:[[User alloc] initWithDictionary:@{@"hue": @"hue"}]];
//        }
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureNavigationBar];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64-49)];
    [_tableView registerClass:[MonitorTableViewCell class] forCellReuseIdentifier:kMonitorCell];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_tableView];
    
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    [searchBar setPlaceholder:@"Pesquise uma especialidade"];
    [searchBar setBarTintColor:[UIColor colorFromHexString:@"EEEEEEE"]];
    [searchBar setSearchBarStyle:UISearchBarStyleProminent];
    [searchBar setDelegate:self];
    [_tableView setTableHeaderView:searchBar];
    
    
    [self loadData];
    
}

#pragma mark - UISearchBar delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if(searchBar.text.length > 0) {
        _filterString = searchBar.text;
    } else {
        _filterString = @"";
    }
    [self.view endEditing:YES];
    [self loadData];

}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if(searchBar.text.length == 0) {
        _filterString = @"";
        [self loadData];
    }
}

#pragma mark - Parse Methods

- (void) loadData {
    
    [_monitorsArray removeAllObjects];
    
    PFQuery *query = [PFQuery queryWithClassName:@"usuario"];
    [query whereKey:@"objectId" notEqualTo:[AppDelegate sharedDelegate].loggedUser.guid];
    
    if(_filterString.length > 0) {
        [query whereKey:@"especialidades" containsString:_filterString];
    }
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        
        if (!error && [objects count] > 0) {
            
            for (PFObject *obj in objects) {
                
                User *usr = [[User alloc] initWithParseObject:obj];
                
                [_monitorsArray addObject:usr];
            }
            
        }
        [_tableView reloadData];

        

    }];
}

#pragma mark - UITableView Datasources

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_monitorsArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    MonitorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMonitorCell forIndexPath:indexPath];
    
    if(!cell) {
        cell = [[MonitorTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMonitorCell];
    }
    [cell layoutSubviews];
    [cell setUser:_monitorsArray[indexPath.row]];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 95.0f;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([_monitorsArray count] > 0) {
        [self.navigationController pushViewController:[[ProfileViewController alloc] initWithUser:_monitorsArray[indexPath.row]] animated:YES];
    }

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}


#pragma mark - Tabbar configuration

- (void)configureNavigationBar {
    self.title = @"Monitores";
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar setBarTintColor:[UIColor furbBlue]];
    [navigationBar setTintColor:[UIColor whiteColor]];
    [navigationBar setTranslucent:NO];
    [navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                            NSFontAttributeName: [UIFont avenirDemiWithSize:18.0f]}];
    
    [[self findHairlineImageViewUnder:navigationBar] setHidden:YES];
}

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

- (void)touchDismiss {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
