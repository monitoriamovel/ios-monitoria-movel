//  MonitoriaMovel
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.


#import <UIKit/UIKit.h>
@class Conversation;
@interface ConversationTableViewCell : UITableViewCell
- (void)setConversation:(Conversation *)conversation;
@end
