//
//  AppDelegate.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 06/04/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "AppDelegate.h"
#import <SendBirdSDK/SendBirdSDK.h>

#import <Parse/Parse.h>
#import "LoginViewController.h"
#import "UIImage+MM.h"
#import "UIColor+MM.h"
#import "RDVTabBarItem.h"


#import "MonitorListViewController.h"
#import "CalendarViewController.h"
#import "ProfileViewController.h"
#import "User.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    
    
    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration>  _Nonnull configuration) {
        // Add your Parse applicationId:
        configuration.applicationId = @"monitoriamovel";
        
        
        // Uncomment and add your clientKey (it's not required if you are using Parse Server):
//        configuration.clientKey = @"monitoria_movel_furb";
        
        // Uncomment the following line and change to your Parse Server address;
        configuration.server = @"https://monitoriamovel.herokuapp.com/parse/";
        
        // Enable storing and querying data from Local Datastore. Remove this line if you don't want to
        // use Local Datastore features or want to use cachePolicy.
        configuration.localDatastoreEnabled = NO;
    }]];

    
    //Configure Chat Service
    [SBDMain initWithApplicationId:@"E54D845E-3EF8-41BC-BAB0-B0CE4045A3F4"];

    
    
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    bool logado = false;
    
    
    if (logado) {
        [self createTabBar];
    } else {
        [self createNavigationController];
        
    } 
    
    
    [self.window makeKeyAndVisible];

    
    return YES;
}

- (void)createNavigationController {
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[LoginViewController alloc] init]];
    [navigationController setNavigationBarHidden:YES];
    [navigationController.navigationBar setTranslucent:NO];
    [navigationController.navigationBar setBarTintColor:[UIColor furbBlue]];
    [navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    [navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor furbBlue]]
                                            forBarPosition:UIBarPositionAny
                                                barMetrics:UIBarMetricsDefault];
    [navigationController.navigationBar setShadowImage:[UIImage new]];
    
    [self.window setRootViewController:navigationController];
    
    
}

- (void) createTabBar {
    UINavigationController *navMonitorList = [[UINavigationController alloc] initWithRootViewController:[[MonitorListViewController alloc] init]];
    UINavigationController *navChats = [[UINavigationController alloc] initWithRootViewController:[[ChatsViewController alloc] init]];
    UINavigationController *navCalendar = [[UINavigationController alloc] initWithRootViewController:[[CalendarViewController alloc] init]];
    UINavigationController *navProfile = [[UINavigationController alloc] initWithRootViewController:[[ProfileViewController alloc] initWithMe]];
//    [navProfile setNavigationBarHidden:YES];
    
    self.tabBarController = [[RDVTabBarController alloc] init];

    [self.tabBarController setViewControllers:@[navMonitorList,navChats, navCalendar, navProfile]];
    
    
    [self setImage:[UIImage imageNamed:@"ic_list"] forTabBarItem:[[self.tabBarController tabBar] items][0]];
    [self setImage:[UIImage imageNamed:@"ic_chat"] forTabBarItem:[[self.tabBarController tabBar] items][1]];
    [self setImage:[UIImage imageNamed:@"ic_calendar"] forTabBarItem:[[self.tabBarController tabBar] items][2]];
    [self setImage:[UIImage imageNamed:@"ic_profile"] forTabBarItem:[[self.tabBarController tabBar] items][3]];
    [self.tabBarController setSelectedIndex:0];
    
    [self.window setRootViewController:self.tabBarController];
}


- (void)setImage:(UIImage *)image forTabBarItem:(RDVTabBarItem *)barItem{
    
    [barItem setBackgroundSelectedImage:[UIImage imageWithColor:[UIColor furbYellow]]
                    withUnselectedImage:[UIImage imageWithColor:[UIColor furbBlue]]];
    
    UIImage *selectedImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIGraphicsBeginImageContextWithOptions(image.size, NO, selectedImage.scale);
    [[UIColor furbBlue] set];
    [selectedImage drawInRect:CGRectMake(0, 0, selectedImage.size.width, selectedImage.size.height)];
    selectedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    UIImage *unSelectedImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIGraphicsBeginImageContextWithOptions(image.size, NO, unSelectedImage.scale);
    [[UIColor whiteColor] set];
    [unSelectedImage drawInRect:CGRectMake(0, 0, unSelectedImage.size.width, unSelectedImage.size.height)];
    unSelectedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [barItem setFinishedSelectedImage:selectedImage
          withFinishedUnselectedImage:unSelectedImage];
    
}

- (void) atualizaInfosUsuarioLogado {

    PFQuery *query = [PFQuery queryWithClassName:@"usuario"];
    
    [query getObjectInBackgroundWithId:_loggedUser.myself.objectId block:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if(!error) {
            _loggedUser = [[User alloc] initWithParseObject:object];
        }
    }];

}

- (ChatsViewController *) getChatViewController {
    return self.tabBarController.viewControllers[1];
}

- (void) selectTabbarAtIndex:(int) idx {
    [self.tabBarController setSelectedIndex:idx];
}

- (void) logout {
    [SBDMain disconnectWithCompletionHandler:^{
    }];
    
    _loggedUser = nil;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"loggedguid"];
    [userDefaults synchronize];
    
    [self createNavigationController];
}


+ (AppDelegate *)sharedDelegate {
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    
    //Desconecta do chat
    [SBDMain disconnectWithCompletionHandler:^{
    }];
    
}


@end
