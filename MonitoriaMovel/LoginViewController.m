//
//  ViewController.m
//  LOL
//
//  Created by Lucas Eduardo Schlogl on 16/07/16.
//  Copyright © 2016 Lucas Eduardo Schlogl. All rights reserved.
//

#import "LoginViewController.h"
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "UIColor+MM.h"
#import "JVFloatLabeledTextField.h"
#import "UIFont+MM.h"
#import "RegisterViewController.h"
#import "ForgotPasswordViewController.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"
#import "CryptCommons.h"


@interface LoginViewController ()
@property (nonatomic, strong) UIImageView *logo;
@property (nonatomic, strong) JVFloatLabeledTextField *fieldLogin;
@property (nonatomic, strong) JVFloatLabeledTextField *fieldPassword;
@property (nonatomic, strong) UIButton *forgotPassword;
@property (nonatomic, strong) UIButton *registerAccount;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor furbBlue]];
    
    float width = self.view.frame.size.width;
    
    _logo = [[UIImageView alloc] initWithFrame:CGRectMake((width-250)/2, 50, 250, 164)];
    [_logo setImage:[UIImage imageNamed:@"logo_furb"]];
    [self.view addSubview:_logo];
    
    _fieldLogin = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(50, _logo.frame.size.height+_logo.frame.origin.y+40, width-100, 40)];
    [self configureTextField:_fieldLogin withString:@"Usuário"];
    [_fieldLogin addTarget:_fieldLogin action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.view addSubview:_fieldLogin];
    _fieldLogin.keepBaseline = YES;

    
    _fieldPassword = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(50, _fieldLogin.frame.size.height+_fieldLogin.frame.origin.y+20, width-100, 40)];
  
    [_fieldPassword setSecureTextEntry:YES];
    [self configureTextField:_fieldPassword withString:@"Senha"];
    
    [_fieldPassword addTarget:_fieldPassword action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.view addSubview:_fieldPassword];
    _fieldPassword.keepBaseline = YES;
    
    
    
    
    _registerAccount = [[UIButton alloc] initWithFrame:CGRectMake(50, _fieldPassword.frame.origin.y+_fieldPassword.frame
                                                                 .size.height+20, 100, 20)];
    [_registerAccount setTitle:@"Registrar-se" forState:UIControlStateNormal];
    [_registerAccount addTarget:self action:@selector(touchRegister) forControlEvents:UIControlEventTouchUpInside];
    [_registerAccount setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_registerAccount.titleLabel setFont:[UIFont avenirLightWithSize:12.0f]];
    [self.view addSubview:_registerAccount];

    
    _forgotPassword = [[UIButton alloc] initWithFrame:CGRectMake(_registerAccount.frame.origin.x+_registerAccount.frame.size.width+10, _fieldPassword.frame.origin.y+_fieldPassword.frame.size.height+20, 100, 20)];
    [_forgotPassword setTitle:@"Esqueci a senha" forState:UIControlStateNormal];
    [_forgotPassword addTarget:self action:@selector(touchForgotPassword) forControlEvents:UIControlEventTouchUpInside];
    [_forgotPassword setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_forgotPassword.titleLabel setFont:[UIFont avenirLightWithSize:12.0f]];
    [self.view addSubview:_forgotPassword];
    
    
    UIButton *buttonLogin = [[UIButton alloc] initWithFrame:CGRectMake(50, _forgotPassword.frame.origin.y+_forgotPassword.frame.size.height+25, width-100, 40)];
    [buttonLogin addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [buttonLogin setTitle:@"Logar" forState:UIControlStateNormal];
    [buttonLogin setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonLogin setBackgroundColor:[UIColor furbYellow]];
    [buttonLogin.layer setCornerRadius:2.0f];
    [self.view addSubview:buttonLogin];

    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if([userDefaults objectForKey:@"loggedguid"]) {
        [self loginCacheWithGuid:[userDefaults objectForKey:@"loggedguid"]];
    }
}

- (void) configureTextField:(JVFloatLabeledTextField *)textField withString:(NSString *) str{
   
    //Field settings
    textField.font = [UIFont avenirRegularWithSize:16.0f];
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str
                                    attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    textField.floatingLabelFont = [UIFont avenirBoldWithSize:11.0f];
    textField.floatingLabelTextColor = [UIColor whiteColor];
    textField.floatingLabelActiveTextColor = [UIColor furbYellow];
    [textField setTextColor:[UIColor whiteColor]];

    
    //Keyboard settings
    [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [textField setReturnKeyType:UIReturnKeyDone];
}

- (void) touchRegister {
    [self.navigationController pushViewController:[[RegisterViewController alloc] init] animated:YES];
}

- (void) touchForgotPassword {
    [self.navigationController pushViewController:[[ForgotPasswordViewController alloc] init] animated:YES];
}

- (void) dismissHud {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // time-consuming task
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
    });
}

- (void) loginCacheWithGuid:(NSString *) guid{

    [SVProgressHUD showWithStatus:@"Carregando suas informações..."];
    

    
    PFQuery *query = [PFQuery queryWithClassName:@"usuario"];
    [query getObjectInBackgroundWithId:guid block:^(PFObject * _Nullable obj, NSError * _Nullable error) {
        [self dismissHud];
        
        if (!error && obj) {
            [AppDelegate sharedDelegate].loggedUser = [[User alloc] initWithParseObject:obj];
            [[AppDelegate sharedDelegate] createTabBar];
            
        } else {
            [SVProgressHUD showErrorWithStatus:@"Sua conexão expirou, entre novamente."];
            //            [[AppDelegate sharedDelegate] logout];
        }

    }];
}

- (void) login {
    [self.view endEditing:YES];
    
//    [AppDelegate sharedDelegate].loggedUser = [[User alloc] initWithDictionary:@{}];
//    [[AppDelegate sharedDelegate] createTabBar];
//    return;
    
    [SVProgressHUD showWithStatus:@"Autenticando..."];
    
    if([_fieldLogin.text length] == 0 || [_fieldPassword.text length] == 0) {
        [self dismissHud];

        [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Preencha todos os dados" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        return;
    }

    
    PFQuery *query = [PFQuery queryWithClassName:@"usuario"];
    [query whereKey:@"senha" equalTo:[CryptCommons md5:_fieldPassword.text]];
    [query whereKey:@"usuario" equalTo:_fieldLogin.text];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {

        [self dismissHud];
        bool logado = false;

        if (!error && [objects count] == 1) {
            PFObject *obj = objects[0];
            
            [[[UIAlertView alloc] initWithTitle:@"Sucesso" message:[NSString stringWithFormat:@"Seja bem-vindo %@", obj[@"nome"]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            logado = true;
            [AppDelegate sharedDelegate].loggedUser = [[User alloc] initWithParseObject:obj];
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:[AppDelegate sharedDelegate].loggedUser.guid forKey:@"loggedguid"];
            [userDefaults synchronize];

//            if([userDefaults objectForKey: obj[@"nome"]]) {
//                
//            }

            
            //Seta no AppDelegate o usuário compartilhado
            [[AppDelegate sharedDelegate] createTabBar];
            return;
        }
        
        
        if (!logado) {
            [[[UIAlertView alloc] initWithTitle:@"Erro" message:@"Usuário ou senha incorreto.\nVerifique as informações." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            return;
        }
    }];
}

@end
