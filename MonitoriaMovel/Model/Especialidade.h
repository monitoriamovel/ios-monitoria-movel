//
//  Especialidade.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface Especialidade : BaseModel
@property (nonatomic, strong, readonly) NSString *guid;
@property (nonatomic, strong, readonly) NSString *descricao;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
