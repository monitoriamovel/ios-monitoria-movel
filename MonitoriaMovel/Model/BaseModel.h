//
//  BaseModel.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject
- (id)valueOrNil:(id)field;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
