//
//  User.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseModel.h"
#import "Especialidade.h"
#import <Parse/PFObject.h>
#import <PArse/PFFile.h>

@interface User : BaseModel
@property (nonatomic, strong, readonly) PFObject *myself;
@property (nonatomic, strong, readonly) NSString *guid;
@property (nonatomic, strong, readonly) NSString *nome;
@property (nonatomic, strong, readonly) NSString *data_nascimento;
@property (nonatomic, strong, readonly) NSString *login;
@property (nonatomic, strong, readonly) NSString *email;
@property (nonatomic, strong, readonly) UIImage *foto;
@property (nonatomic, strong, readonly) PFFile *fotoFile;
@property (nonatomic, assign, readonly) float avaliacaoMedia;
@property (nonatomic, assign, readonly) bool isMonitorVerificado;
@property (nonatomic, strong, readonly) NSString *especialidades;

- (instancetype)initWithParseObject:(PFObject *)object;
- (void)getFotoWithcompletionBlock:(void (^)(UIImage *image))completionBlock;
- (PFObject *) getParseObject;
- (NSString *)formattedSkills;
@end
