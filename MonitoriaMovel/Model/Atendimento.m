//
//  Atendimento.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 06/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "Atendimento.h"
#import "DateFormatter.h"

@interface Atendimento()
@property (nonatomic, strong) PFObject *myself;
@property (nonatomic, strong) NSString *guid;
@property (nonatomic, strong) NSDate *data_inicio;
@property (nonatomic, strong) NSDate *data_fim;
@property (nonatomic, assign) int status;
@property (nonatomic, assign) bool horarioAceito;

@property (nonatomic, strong) NSDate *horarioMarcado;


@property (nonatomic, strong) User *monitor;
@property (nonatomic, strong) User *usuario; //quem abriu o chamado

@property (nonatomic, assign) float avaliacao; //avaliacao desse atendimento

@end

@implementation Atendimento


- (instancetype)initWithParseObject:(PFObject *)object
{
    self = [super init];
    if (self) {
        _myself = object;
        self.guid = object.objectId;
        if(object[@"data_inicio"])
            self.data_inicio = object[@"data_inicio"];
        
        if(object[@"data_fim"])
            self.data_fim = object[@"data_fim"];
        
        self.horarioMarcado = object[@"horario_marcado"];
        // 0 = pendente pro monitor verificar se pode ou n
        // 1 = aceito o horário
        // 2 = rejeitado -> ver um novo horário
        self.status = [object[@"status"] intValue];
        
        PFObject *monitor = object[@"monitor"];
        PFObject *usuario = object[@"usuario"];
        
        self.monitor = [[User alloc] initWithParseObject:monitor];
        self.usuario = [[User alloc] initWithParseObject:usuario];
        self.horarioAceito = [object[@"horario_aceito"] boolValue];
        
        self.avaliacao = [object[@"avaliacao"] floatValue];
        
    }
    return self;
}

- (NSString *) dataHorarioMarcadoFormatada {
    return [[DateFormatter sharedFormatter] diaHoraFormatter:_horarioMarcado];
}

@end
