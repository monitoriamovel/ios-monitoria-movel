//
//  Especialidade.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "Especialidade.h"

@interface Especialidade ()
@property (nonatomic, strong) NSString *guid;
@property (nonatomic, strong) NSString *descricao;
@end

@implementation Especialidade

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if(!dictionary)
        return nil;
    
    if(self = [super init])
    {
        self.guid = [self valueOrNil: dictionary[@"guid"]];
        self.descricao = [self valueOrNil: dictionary[@"descricao"]];
        
    }
    return self;
}
@end
