//
//  User.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "User.h"

@interface User ()
@property (nonatomic, strong) PFObject *myself;
@property (nonatomic, strong) NSString *guid;
@property (nonatomic, strong) NSString *nome;
@property (nonatomic, strong) NSString *data_nascimento;
@property (nonatomic, strong) NSString *login;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) UIImage *foto;
@property (nonatomic, strong) PFFile *fotoFile;
@property (nonatomic, assign) bool isMonitorVerificado;
@property (nonatomic, assign) float avaliacaoMedia;
//@property (nonatomic, strong) NSArray<Especialidade *> *especialidades;
@property (nonatomic, strong) NSString *especialidades;

@end

@implementation User

- (instancetype)initWithParseObject:(PFObject *)object
{
    self = [super init];
    if (self) {
        self.myself = object;
        self.guid = object.objectId;
        self.nome = object[@"nome"];
        self.login = object[@"usuario"];
        self.email = object[@"email"];
        self.data_nascimento = object[@"data_nascimento"];
        self.especialidades = object[@"especialidades"];
        self.fotoFile =  object[@"foto"];
        self.isMonitorVerificado = [object[@"verificado"] boolValue];
        self.avaliacaoMedia = [object[@"avaliacao"] floatValue];
        
    }
    return self;
}

- (void)getFotoWithcompletionBlock:(void (^)(UIImage *image))completionBlock {
    
    if(_fotoFile)
        [_fotoFile getDataInBackgroundWithBlock:^(NSData * _Nullable data, NSError * _Nullable error) {
            if(!data) {
                NSLog(@"imagem nao carregou");
              self.foto = [UIImage imageNamed:@"user_no_photo"];
            }
            self.foto = [UIImage imageWithData:data];
        
            
            completionBlock(self.foto);
        }];
    

}

- (PFObject *) getParseObject {
    PFObject *obj = [[PFObject alloc] initWithClassName:@"usuario"];
    obj.objectId = self.guid;
    obj[@"nome"] = self.nome;
    obj[@"usuario"] = self.login;
    obj[@"email"] = self.email;
    obj[@"data_nascimento"] = self.data_nascimento;
    obj[@"especialidades"] = self.especialidades;
    obj[@"foto"] = self.fotoFile;
    if(self.isMonitorVerificado) {
        obj[@"verificado"] = @(1);
    } else {
        obj[@"verificado"] = @(0);
    }
    obj[@"avaliacao"] = @(self.avaliacaoMedia);
    
    return obj;
}


- (NSString *)formattedSkills {
    return self.especialidades;
}

@end
