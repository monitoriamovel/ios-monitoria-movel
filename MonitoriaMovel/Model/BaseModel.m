//
//  BaseModel.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel
- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    return [super init];
}


- (id)valueOrNil:(id)field {
    if([field isKindOfClass:[NSNull class]])
        return nil;
    
    return field;
}

@end
