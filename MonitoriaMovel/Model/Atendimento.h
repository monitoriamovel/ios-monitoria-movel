//
//  Atendimento.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 06/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"
#import <Parse/PFObject.h>
#import "User.h"

@interface Atendimento : BaseModel
@property (nonatomic, strong, readonly) PFObject *myself;
@property (nonatomic, strong, readonly) NSString *guid;
@property (nonatomic, strong, readonly) NSDate *data_inicio;
@property (nonatomic, strong, readonly) NSDate *data_fim;
@property (nonatomic, assign, readonly) int status;

@property (nonatomic, strong, readonly) NSDate *horarioMarcado;


@property (nonatomic, strong, readonly) User *monitor;
@property (nonatomic, strong, readonly) User *usuario; //quem abriu o chamado

@property (nonatomic, assign, readonly) float avaliacao; //avaliacao desse atendimento

- (instancetype)initWithParseObject:(PFObject *)object;
- (NSString *) dataHorarioMarcadoFormatada;
@end
