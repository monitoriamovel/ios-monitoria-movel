//
//  AppDelegate.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 06/04/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVTabBarController.h"
#import "User.h"
#import "ChatsViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, strong) RDVTabBarController *tabBarController;
@property (nonatomic, strong) User *loggedUser;

+ (AppDelegate *)sharedDelegate;
- (void) createTabBar;
- (void) createNavigationController;
- (void) logout;
- (void) atualizaInfosUsuarioLogado;
- (void) selectTabbarAtIndex:(int) idx;
- (ChatsViewController *) getChatViewController;
@end

