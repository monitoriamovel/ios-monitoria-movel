//  MonitoriaMovel
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.


#import "ConversationTableViewCell.h"
#import "UIColor+MM.h"
#import "UIFont+MM.h"

//#import "Conversation.h"
#import "User.h"
//#import "Message.h"
#import "DateFormatter.h"
//#import "CCBufferedImageView.h"
@interface ConversationTableViewCell()
//@property (nonatomic, strong) CCBufferedImageView *profileImageView;
@property (nonatomic, strong) UIImageView *profileImageView;
@property (nonatomic, strong) UILabel *userLabel;
@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *unreadMessagesLabel;
@end
@implementation ConversationTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        self.profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 40, 40)];
        [self.profileImageView.layer setCornerRadius:20.0f];
        [self.profileImageView setImage:[UIImage imageNamed:@"user_no_photo"]];
        [self.profileImageView setClipsToBounds:YES];
        [self.contentView addSubview:self.profileImageView];
        
        self.userLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, 200, 20)];
        [self.userLabel setFont:[UIFont avenirDemiWithSize:15.0f]];
        [self.userLabel setTextColor:[UIColor furbBlue]];
        [self.contentView addSubview:self.userLabel];
        
        
        self.messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 30, 200, 20)];
        [self.messageLabel setFont:[UIFont avenirRegularWithSize:14.0f]];
        [self.messageLabel setTextColor:[UIColor textLightGray]];
        [self.contentView addSubview:self.messageLabel];
        
        self.timeLabel = [UILabel new];
        [self.timeLabel setFont:[UIFont avenirRegularWithSize:10.0f]];
        [self.timeLabel setTextColor:[UIColor textLightGray]];
        [self.timeLabel setTextAlignment:NSTextAlignmentRight];
        [self.contentView addSubview:self.timeLabel];
        
        self.unreadMessagesLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 5, 20, 20)];
        [self.unreadMessagesLabel setBackgroundColor:[UIColor furbBlue]];
        [self.unreadMessagesLabel setTextAlignment:NSTextAlignmentCenter];
        [self.unreadMessagesLabel setFont:[UIFont avenirLightWithSize:10.0f]];
        [self.unreadMessagesLabel setTextColor:[UIColor whiteColor]];
        [self.unreadMessagesLabel.layer setCornerRadius:10.0f];
        [self.unreadMessagesLabel.layer setBorderColor:[UIColor whiteColor].CGColor];
        [self.unreadMessagesLabel.layer setBorderWidth:1.0f];
        [self.unreadMessagesLabel setClipsToBounds:YES];
        [self.contentView addSubview:self.unreadMessagesLabel];
    }
    return self;
}

- (void)setConversation:(Conversation *)conversation {
//    [self.userLabel setText:conversation.user.name];
//    
//    
//    if(conversation.user.profile_image && [conversation.user.profile_image length] > 0)
//        [self.profileImageView load:[NSURL URLWithString:conversation.user.profile_image]];
//    else
//        [self.profileImageView setImage:nil];
//    
//    [self.messageLabel setText:conversation.lastMessage.message];
//    [self.timeLabel setText:[[DateFormatter sharedFormatter] daysToDate:conversation.lastMessage.date]];
//    if(conversation.unreadMessages > 0) {
//        [self.unreadMessagesLabel setText:[NSString stringWithFormat:@"%d", conversation.unreadMessages]];
//        [self.unreadMessagesLabel setAlpha:1.0f];
//    }else {
//        [self.unreadMessagesLabel setAlpha:0.0f];
//    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.contentView.frame.size.width;
    [self.userLabel setFrame:CGRectMake(70, 10, width-70-65, 20)];
    [self.messageLabel setFrame:CGRectMake(70, 30, width-70-65, 20)];
    [self.timeLabel setFrame:CGRectMake(width-60, 10, 50, 20)];
    
    
}

@end
