//
//  AvaliaAtendimentoViewController.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 21/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Atendimento.h"

@interface AvaliaAtendimentoViewController : UIViewController
- (instancetype)initWithAtendimento:(Atendimento *) atendimento;
@end
