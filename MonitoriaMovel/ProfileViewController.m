//
//  ProfileViewController.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "ProfileViewController.h"
#import "UIFont+MM.h"
#import "UIColor+MM.h"
#import "ImageProvider.h"
#import "AppDelegate.h"
#import "RegisterViewController.h"
#import <SendBirdSDK/SendBirdSDK.h>
#import "NewAtendimentoViewController.h"
#import "MudarSenhaViewController.h"
//#import "AvaliaAtendimentoViewController.h"

@interface ProfileViewController ()<UIActionSheetDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) User *user;
@property (nonatomic, assign) BOOL       isMe;
//CCBufferedImageView
@property (nonatomic, strong) UIImageView *profileImageView;
@property (nonatomic, strong) UILabel *ratingLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *skillsLabel;
@property (nonatomic, strong) UILabel *verifiedLabel;
@end

@implementation ProfileViewController

- (instancetype)initWithUser:(User *) user
{
    self = [super init];
    if (self) {
        _isMe = false;
        _user = user;
    }
    return self;
}

- (instancetype) initWithMe {
    self = [super init];
    if (self) {
        _isMe = true;
        _user = [AppDelegate sharedDelegate].loggedUser;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureNavigationBar];
    [self reloadView];
}


- (void)reloadView {
    while ([[self.view subviews] count] > 0)
    {
        [[self.view subviews][0] removeFromSuperview];
    }
    
    if(self.isMe) {
        self.user = [AppDelegate sharedDelegate].loggedUser;
    } else {
        UIImage *imgBack = [[UIImage imageNamed:@"ic_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:imgBack
                                                                                   style:UIBarButtonItemStylePlain
                                                                                  target:self action:@selector(dismiss)]];
    }
    
    CGFloat width = CGRectGetWidth(self.view.frame);
    
    UIView *topBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 52, width, 300)];
    [topBackgroundView setBackgroundColor:[UIColor colorFromHexString:@"f7f7f7"]];
    [self.view addSubview:topBackgroundView];
    
    
    
    
//    CCBufferedImageView *profileImageView = [[CCBufferedImageView alloc] initWithFrame:CGRectMake((width-84)/2.0f, 10, 84, 84)];
    UIImageView *profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake((width-84)/2.0f, 10, 84, 84)];
    [self.user getFotoWithcompletionBlock:^(UIImage *image) {
        [profileImageView setImage:image];
    }];
    
    [profileImageView.layer setCornerRadius:42.0f];
    [profileImageView setClipsToBounds:YES];
    [profileImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:profileImageView];
    
    
    
    
    NSMutableAttributedString *ratingText = [[NSMutableAttributedString alloc] init];
    
    UIImage *ic_star = [UIImage imageNamed:@"ic_star"];
    NSTextAttachment *ic_starAttach = [[NSTextAttachment alloc] init];
    [ic_starAttach setImage: ic_star];
    [ic_starAttach setBounds: CGRectMake(0, -1, ic_star.size.width, ic_star.size.height)];
    
    NSString *rating = @" N/A";
    if (self.user.avaliacaoMedia && self.user.avaliacaoMedia > 0)
        rating = [NSString stringWithFormat:@" %.1f", self.user.avaliacaoMedia];
    
    [ratingText appendAttributedString:[NSAttributedString attributedStringWithAttachment:ic_starAttach]];
    [ratingText appendAttributedString:[[NSAttributedString alloc] initWithString:rating
                                                                       attributes:@{
                                                                                    NSFontAttributeName: [UIFont avenirDemiWithSize:30.0f],
                                                                                    NSForegroundColorAttributeName: [UIColor furbBlue]
                                                                                    }]];
    
    UILabel *ratingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 109, width, 35)];
    [ratingLabel setTextAlignment:NSTextAlignmentCenter];
    [ratingLabel setAttributedText:ratingText];
    [self.view addSubview:ratingLabel];

    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 150, width-20, 20)];
    [nameLabel setText:self.user.nome];
    [nameLabel setFont:[UIFont avenirDemiWithSize:16.0f]];
    [nameLabel setTextColor:[UIColor furbBlue]];
    [nameLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:nameLabel];
    
    
    UILabel *skillsLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 175, width-20, 100)];
    [skillsLabel setText:[self.user formattedSkills]];
    [skillsLabel setFont:[UIFont avenirRegularWithSize:15.0f]];
    [skillsLabel setTextColor:[UIColor furbBlue]];
    [skillsLabel setNumberOfLines:4];
    [skillsLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:skillsLabel];
    [skillsLabel sizeToFit];
    [skillsLabel setFrame:CGRectMake(10, 175, width-20, CGRectGetHeight(skillsLabel.frame))];
    
    
    
    UILabel *verifiedLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(skillsLabel.frame)+5, width-20, 20)];
    [verifiedLabel setText:self.user.isMonitorVerificado ? @"Monitor concursado" : @"Monitor voluntário"];
    [verifiedLabel setFont:[UIFont avenirRegularWithSize:15.0f]];
    [verifiedLabel setTextColor:[UIColor darkTextColor]];
    [verifiedLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:verifiedLabel];
    

    
    [topBackgroundView setFrame:CGRectMake(0, 52, width, CGRectGetMaxY(verifiedLabel.frame)-52+20)];
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:topBackgroundView.bounds
                                                   byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                                         cornerRadii:CGSizeMake(18.0, 18.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = topBackgroundView.bounds;
    maskLayer.path = maskPath.CGPath;
    topBackgroundView.layer.mask = maskLayer;
    
    CGFloat nextY = CGRectGetHeight(topBackgroundView.frame)+topBackgroundView.frame.origin.y;
    
    
    UIView *buttonsView = [[UIView alloc] initWithFrame:CGRectMake(0, nextY, width, [UIScreen mainScreen].bounds.size.height-nextY-115)];
    [self.view addSubview:buttonsView];
    
    if(self.isMe) {
        [self addButtonsForMeInView:buttonsView];
        self.title = @"Meu perfil";
    } else {
        [self addButtonsForProfileInView:buttonsView];
        self.title = self.user.nome;
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    

    
    [self reloadView];
    
}

- (void)addButtonsForMeInView:(UIView *)view {
    CGFloat width = CGRectGetWidth(view.frame);
    CGFloat height = CGRectGetHeight(view.frame);
    
    
    NSMutableAttributedString *editAttributedText = [NSMutableAttributedString new];
    UIImage *ic_edit = [UIImage imageNamed:@"ic_edit_profile"];
    NSTextAttachment *ic_editAttach = [[NSTextAttachment alloc] init];
    [ic_editAttach setImage: ic_edit];
    [ic_editAttach setBounds: CGRectMake(0, -5, ic_edit.size.width, ic_edit.size.height)];
    
    [editAttributedText appendAttributedString:[NSAttributedString attributedStringWithAttachment:ic_editAttach]];
    [editAttributedText appendAttributedString:[[NSAttributedString alloc] initWithString:@" Editar perfil"
                                                                               attributes:@{
                                                                                            NSFontAttributeName: [UIFont avenirRegularWithSize:14.0f],
                                                                                            NSForegroundColorAttributeName: [UIColor furbBlue]
                                                                                            }]];
    
    UIButton *btnEdit = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width/2.0f, height)];
    [btnEdit setAttributedTitle:editAttributedText forState:UIControlStateNormal];
    [btnEdit addTarget:self action:@selector(touchEditProfile) forControlEvents:UIControlEventTouchUpInside];
    [btnEdit.titleLabel setNumberOfLines:0];
    [btnEdit.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [view addSubview:btnEdit];
    
    NSMutableAttributedString *settingsAttributedText = [NSMutableAttributedString new];
    UIImage *ic_settings = [UIImage imageNamed:@"ic_config"];
    NSTextAttachment *ic_settingsAttach = [[NSTextAttachment alloc] init];
    [ic_settingsAttach setImage: ic_settings];
    [ic_settingsAttach setBounds: CGRectMake(0, -5, ic_settings.size.width, ic_settings.size.height)];
    
    [settingsAttributedText appendAttributedString:[NSAttributedString attributedStringWithAttachment:ic_settingsAttach]];
    [settingsAttributedText appendAttributedString:[[NSAttributedString alloc] initWithString:@" Configurações"
                                                                                   attributes:@{
                                                                                                NSFontAttributeName: [UIFont avenirRegularWithSize:14.0f],
                                                                                                NSForegroundColorAttributeName: [UIColor furbBlue]
                                                                                                }]];
    
    UIButton *btnSettings = [[UIButton alloc] initWithFrame:CGRectMake(width/2.0f, 0, width/2.0f, height)];
    [btnSettings setAttributedTitle:settingsAttributedText forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(touchSettings) forControlEvents:UIControlEventTouchUpInside];
    [btnSettings.titleLabel setNumberOfLines:0];
    [btnSettings.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [view addSubview:btnSettings];
    
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(width/2.0f, height*0.3f, 1, height*0.4f)];
    [separator setBackgroundColor:[UIColor furbBlue]];
    [view addSubview:separator];
}

- (void)addButtonsForProfileInView:(UIView *)view {
    CGFloat width = CGRectGetWidth(view.frame);
    CGFloat height = CGRectGetHeight(view.frame);
    
    
    NSMutableAttributedString *messageAttributedText = [NSMutableAttributedString new];
    UIImage *ic_message = [UIImage imageNamed:@"ic_message_profile"];
    NSTextAttachment *ic_messageAttach = [[NSTextAttachment alloc] init];
    [ic_messageAttach setImage: ic_message];
    [ic_messageAttach setBounds: CGRectMake(0, -5, ic_message.size.width, ic_message.size.height)];
    
    [messageAttributedText appendAttributedString:[NSAttributedString attributedStringWithAttachment:ic_messageAttach]];
    [messageAttributedText appendAttributedString:[[NSAttributedString alloc] initWithString:@" Mensagem"
                                                                                  attributes:@{
                                                                                               NSFontAttributeName: [UIFont avenirRegularWithSize:14.0f],
                                                                                               NSForegroundColorAttributeName: [UIColor furbBlue]
                                                                                               }]];
    
    UIButton *btnMessage = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width/2.0f, height)];
    [btnMessage setAttributedTitle:messageAttributedText forState:UIControlStateNormal];
    [btnMessage addTarget:self action:@selector(touchMessages) forControlEvents:UIControlEventTouchUpInside];
    [btnMessage.titleLabel setNumberOfLines:0];
    [btnMessage.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [view addSubview:btnMessage];
    
    NSMutableAttributedString *calendarAttributedText = [NSMutableAttributedString new];
    UIImage *ic_calendar = [UIImage imageNamed:@"ic_calendar_profile"];
    NSTextAttachment *ic_calendarAttach = [[NSTextAttachment alloc] init];
    [ic_calendarAttach setImage: ic_calendar];
    [ic_calendarAttach setBounds: CGRectMake(0, -5, ic_calendar.size.width, ic_calendar.size.height)];
    
    [calendarAttributedText appendAttributedString:[NSAttributedString attributedStringWithAttachment:ic_calendarAttach]];
    [calendarAttributedText appendAttributedString:[[NSAttributedString alloc] initWithString:@" Agendamento"
                                                                                  attributes:@{
                                                                                               NSFontAttributeName: [UIFont avenirRegularWithSize:14.0f],
                                                                                               NSForegroundColorAttributeName: [UIColor furbBlue]
                                                                                               }]];
    
    UIButton *btnCalendar = [[UIButton alloc] initWithFrame:CGRectMake(width/2.0f, 0, width/2.0f, height)];
    [btnCalendar setAttributedTitle:calendarAttributedText forState:UIControlStateNormal];
    [btnCalendar addTarget:self action:@selector(touchCalendar) forControlEvents:UIControlEventTouchUpInside];
    [btnCalendar.titleLabel setNumberOfLines:0];
    [btnCalendar.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [view addSubview:btnCalendar];
    
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(width/2.0f, height*0.3f, 1, height*0.4f)];
    [separator setBackgroundColor:[UIColor furbBlue]];
    [view addSubview:separator];
    
}


- (void)dismiss {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchEditProfile {
    [self.navigationController pushViewController:[[RegisterViewController alloc] initWithUser:self.user] animated:YES];
}

- (void)touchSettings {
    [[[UIActionSheet alloc] initWithTitle:@"Configurações"
                                 delegate:self
                        cancelButtonTitle:@"Cancelar"
                   destructiveButtonTitle:@"Deslogar" otherButtonTitles:@"Alterar senha", nil] showInView:self.view];
}

- (void)touchMessages {
    [SBDGroupChannel createChannelWithUserIds:@[[AppDelegate sharedDelegate].loggedUser.guid, self.user.guid] isDistinct:YES completionHandler:^(SBDGroupChannel * _Nullable channel, SBDError * _Nullable error) {

        if (error != nil) {
            NSLog(@"Error: %@", error);
            return;
        }

        //Open the next screen :D
        
        //TODO: Arrumar aqui o esquema pra selecionar a tabbar 1 e abrir a conversa com o usuario
        /*
        SBDUser *user;
    
        for (SBDUser *usr in channel.members) {
            if(![usr.userId isEqualToString:[AppDelegate sharedDelegate].loggedUser.guid]) {
                user = usr;
            }
        }


        
        ChatsViewController *chatVC = [[AppDelegate sharedDelegate] getChatViewController];
        [chatVC openChatScreenWithChannel:channel andUser:user];
         */
        [[AppDelegate sharedDelegate] selectTabbarAtIndex:1];
        
    }];
}

- (void)touchCalendar {
    [self.navigationController pushViewController:[[NewAtendimentoViewController alloc] initWithMonitorSelecionado:self.user] animated:YES];
//    [self.navigationController pushViewController:[[AvaliaAtendimentoViewController alloc] init] animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Você tem certeza que deseja deslogar?" delegate:self cancelButtonTitle:@"Não" otherButtonTitles:@"Sim", nil] show];
    } else if(buttonIndex == 1) {
        [self.navigationController pushViewController:[[MudarSenhaViewController alloc] init] animated:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 1)
    {
        [[AppDelegate sharedDelegate] logout];
    }
}


#pragma mark - Tabbar configuration

- (void)configureNavigationBar {
    //self.title = @"Perfil";
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar setBarTintColor:[UIColor furbBlue]];
    [navigationBar setTintColor:[UIColor whiteColor]];
    [navigationBar setTranslucent:NO];
    [navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                            NSFontAttributeName: [UIFont avenirDemiWithSize:18.0f]}];
    
    [[self findHairlineImageViewUnder:navigationBar] setHidden:YES];
}

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

@end
