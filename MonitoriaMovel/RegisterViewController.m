//
//  RegisterViewController.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 09/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "RegisterViewController.h"
#import "UIFont+MM.h"
#import "UIColor+MM.h"
#import "UIImage+MM.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"
#import "VMaskTextField.h"
#import "SVProgressHUD.h"
#import <Parse/Parse.h>
#import "CryptCommons.h"
#import "Services.h"
#import "AppDelegate.h"


@interface RegisterViewController ()<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *fotoImgView;
@property (nonatomic, strong) JVFloatLabeledTextField *nomeField;
@property (nonatomic, strong) VMaskTextField *dtNascimentoField;
@property (nonatomic, strong) JVFloatLabeledTextField *emailField;
@property (nonatomic, strong) JVFloatLabeledTextField *loginField;
@property (nonatomic, strong) JVFloatLabeledTextField *senhaField;
@property (nonatomic, strong) JVFloatLabeledTextView *especialidadesField;

@property (nonatomic, strong) UIButton *registerButton;
@property (nonatomic, strong) User *user;

@property (nonatomic, assign) bool changedPhoto;
@end

@implementation RegisterViewController

- (instancetype)initWithUser:(User *) user
{
    self = [super init];
    if (self) {
        _user = user;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createUI];

    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.view setBackgroundColor:[UIColor colorFromHexString:@"f2f2f2"]];
 
    if(!self.user) {
        [self createNavigationBar];
    }
    

}

- (void) createUI {
    
    float topMargin = self.user ? 0 : 64;
    float bottomMargin = self.user ? 44+64 : 0;
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, topMargin, self.view.frame.size.width, self.view.frame.size.height-topMargin-bottomMargin)];
    [_scrollView setContentInset:UIEdgeInsetsMake(0, 0, 180, 0)];
    [self.view addSubview:_scrollView];
    
//    [_scrollView setBackgroundColor:[UIColor purpleColor]];

    
    float width = _scrollView.frame.size.width;
    float height = _scrollView.frame.size.height;

    
    int y = 20;
    
    _fotoImgView = [[UIImageView alloc] initWithFrame:CGRectMake((width-85)/2, y, 85, 85)];
    [_fotoImgView setImage:[UIImage imageNamed:@"user_no_photo"]];
    [_fotoImgView.layer setCornerRadius:_fotoImgView.frame.size.width/2.0f];
    [_fotoImgView setClipsToBounds:YES];
    [_fotoImgView setContentMode:UIViewContentModeScaleAspectFill];
    [_scrollView addSubview:_fotoImgView];
    
    UIButton *btnFoto = [[UIButton alloc] initWithFrame:_fotoImgView.frame];
    [btnFoto addTarget:self action:@selector(touchSelectPhoto) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:btnFoto];
    
    y += 125;
    
    _nomeField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [self configureTextField:_nomeField withString:@"Nome completo"];
    [_nomeField addTarget:_nomeField action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [_scrollView addSubview:_nomeField];
    _nomeField.keepBaseline = YES;
    
    y += 60;
    
    _dtNascimentoField = [[VMaskTextField alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [_dtNascimentoField setMask:@"##/##/####"];
    [_dtNascimentoField setDelegate:self];
    [self configureTextField:_dtNascimentoField withString:@"Data de nascimento"];
    [_dtNascimentoField addTarget:_dtNascimentoField action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [_dtNascimentoField setKeyboardType:UIKeyboardTypeDecimalPad];
    [_scrollView addSubview:_dtNascimentoField];
    _dtNascimentoField.keepBaseline = YES;
    
    y += 60;
    
    _emailField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [self configureTextField:_emailField withString:@"E-mail"];
    [_emailField addTarget:_emailField action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [_scrollView addSubview:_emailField];
    [_emailField setKeyboardType:UIKeyboardTypeEmailAddress];
    _emailField.keepBaseline = YES;
    
    
    y += 60;
    
    _loginField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [self configureTextField:_loginField withString:@"Login"];
    [_loginField addTarget:_loginField action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [_scrollView addSubview:_loginField];
    _loginField.keepBaseline = YES;
    
    y += 60;
    
    _senhaField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [self configureTextField:_senhaField withString:@"Senha"];
    [_senhaField addTarget:_senhaField action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [_scrollView addSubview:_senhaField];
    _senhaField.keepBaseline = YES;
    [_senhaField setSecureTextEntry:YES];
    
    y += 60;
    
    
    
    _especialidadesField = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(30, y, width-60, 110)];
    [self configureTextView:_especialidadesField withString:@"Especialidades"];
    [_scrollView addSubview:_especialidadesField];
    
    
    UIView *accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    [accessoryView setBackgroundColor:[UIColor colorWithWhite:0.95 alpha:0.8]];
    
    UIButton *btnOK = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-80, 0, 80, 50)];
    [btnOK setTitle:@"OK" forState:UIControlStateNormal];
    [btnOK addTarget:_especialidadesField action:@selector(resignFirstResponder) forControlEvents:UIControlEventTouchUpInside];
    [btnOK setTitleColor:[UIColor furbBlue] forState:UIControlStateNormal];
    [accessoryView addSubview:btnOK];
    
    [_especialidadesField setInputAccessoryView:accessoryView];
    
    
    y += 130;
    
    
    _registerButton = [[UIButton alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [_registerButton addTarget:self action:@selector(touchRegister) forControlEvents:UIControlEventTouchUpInside];
    [_registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_registerButton setBackgroundColor:[UIColor furbBlue]];
    [_registerButton.layer setCornerRadius:2.0f];
    [_registerButton setTitle:@"Registrar-se" forState:UIControlStateNormal];
    [_scrollView addSubview:_registerButton];
 
    
    [_scrollView setContentSize:CGSizeMake(width, y+60)];
    
    if(self.user) {
        self.title = @"Meu perfil";
        UIImage *imgBack = [[UIImage imageNamed:@"ic_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:imgBack
                                                                                   style:UIBarButtonItemStylePlain
                                                                                  target:self action:@selector(touchDismiss)]];

        [_registerButton setTitle:@"Salvar" forState:UIControlStateNormal];

        
        [self.user getFotoWithcompletionBlock:^(UIImage *image) {
            [_fotoImgView setImage:image];
        }];
        
        
        [_nomeField setText:self.user.nome];
        [_dtNascimentoField setText:self.user.data_nascimento];
        [_emailField setText:self.user.email];
        [_loginField setText:self.user.login];
        [_loginField setUserInteractionEnabled:NO];
        [_loginField setEnabled:NO];
        
        [_senhaField setUserInteractionEnabled:NO];
        [_senhaField setEnabled:NO];
        [_senhaField setText:@"senhaaleatoria"];
        [_especialidadesField setText:[self.user formattedSkills]];
    }
    
}

#pragma mark - Photo methods

- (void) touchSelectPhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setAllowsEditing:false];
    [picker setSourceType:UIImagePickerControllerSourceTypeCamera|UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:picker animated:YES completion:NULL];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    if(image) {
        [_fotoImgView setImage:image];
        _changedPhoto = YES;
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark - Register methods


- (void) touchRegister {
    
    //Editando
    if(self.user) {
        
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        
        if(![emailTest evaluateWithObject:_emailField.text])
        {
            [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"E-mail inválido!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            return;
        }
        
        [SVProgressHUD showWithStatus:@"Salvando..."];

        PFQuery *query = [PFQuery queryWithClassName:@"usuario"];
        
        [query getObjectInBackgroundWithId:self.user.myself.objectId block:^(PFObject * _Nullable object, NSError * _Nullable error) {
            [SVProgressHUD dismiss];
            
            if(!error) {
                
                if(_changedPhoto) {
                    PFFile *usrImage = [PFFile fileWithData:UIImageJPEGRepresentation(_fotoImgView.image, 1.0f)];
                    object[@"foto"] = usrImage;
                }
                
                object[@"nome"] = _nomeField.text;
                object[@"data_nascimento"] = _dtNascimentoField.text;
                object[@"email"] = _emailField.text;
                object[@"especialidades"] = _especialidadesField.text;
                
                
                [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    
                    [SVProgressHUD showSuccessWithStatus:@"Sucesso!" ];
                    [[AppDelegate sharedDelegate] atualizaInfosUsuarioLogado];
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Ocorreu um erro.\nTente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            }
        }];
        
    } else {
    //Criando usuario
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        
        
        if ([[_nomeField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0 ||
            [[_dtNascimentoField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0 ||
            [[_emailField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0 ||
            [[_loginField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0 ||
            [[_senhaField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0 ||
            [[_especialidadesField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0 ||
            _fotoImgView.image == nil){
            
            [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Todos os campos são obrigatórios." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            return;
        }
        
        if(![emailTest evaluateWithObject:_emailField.text])
        {
            [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"E-mail inválido!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            return;
        }
        
        [SVProgressHUD showWithStatus:@"Criando..."];

        //VALIDAR AQUI NOME DE USUARIO
        
        
        PFQuery *query = [PFQuery queryWithClassName:@"usuario"];
        [query whereKey:@"usuario" equalTo:_loginField.text];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if(error) {
                
                [SVProgressHUD dismiss];
                
                [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Ocorreu um erro.\nTente novamente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
                
                return;
            }
            
            if ([objects count] > 0) {
                // Ja tem um usuario, não pode ter outro
                
                [SVProgressHUD dismiss];
                
                [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"O login informado já existe.\nEscolha outro." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];

                
                return;
            }
            
            
            //CRIA UM NOVO USUARIO :)
            
            
            //VALIDAR AQUI DATA DE NASCIMENTO :D
            
            
            
            PFFile *usrImage = [PFFile fileWithData:UIImageJPEGRepresentation(_fotoImgView.image, 1.0f)];
            
            PFObject *usr = [PFObject objectWithClassName:@"usuario"];
            usr[@"foto"] = usrImage;
            usr[@"nome"] = _nomeField.text;
            usr[@"data_nascimento"] = _dtNascimentoField.text;
            usr[@"email"] = _emailField.text;
            usr[@"usuario"] = _loginField.text;
            usr[@"senha"] = [CryptCommons md5:_senhaField.text];
            usr[@"especialidades"] = _especialidadesField.text;
            usr[@"verificado"] = @(0);
            usr[@"avaliacao"] = @(0.0);
            
            
            [usr saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    [_nomeField setText:@""];
                    [_dtNascimentoField setText:@""];
                    [_emailField setText:@""];
                    [_loginField setText:@""];
                    [_senhaField setText:@""];
                    [_especialidadesField setText:@""];
                    [_fotoImgView setImage:[UIImage imageNamed:@"user_no_photo"]];
                    
                    [SVProgressHUD showSuccessWithStatus:@"Sucesso!" ];
                    [self.navigationController popViewControllerAnimated:YES];

                } else {
                    [SVProgressHUD dismiss];
                    
                    [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Ocorreu um erro, tente novamente!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                }
                
            }];
            
            
        }];
        
        
    }
    
}

- (void) configureTextField:(JVFloatLabeledTextField *)textField withString:(NSString *) str{
    
    //Field settings
    textField.font = [UIFont avenirRegularWithSize:16.0f];
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str
                                                                      attributes:@{NSForegroundColorAttributeName: [UIColor furbBlue]}];
    textField.floatingLabelFont = [UIFont avenirBoldWithSize:11.0f];
    textField.floatingLabelTextColor = [UIColor furbBlue];
    textField.floatingLabelActiveTextColor = [UIColor furbYellow];
    [textField setTextColor:[UIColor furbBlue]];
    
    
    //Keyboard settings
    [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [textField setReturnKeyType:UIReturnKeyDone];
}

- (void) configureTextView:(JVFloatLabeledTextView *)textView withString:(NSString *) str{
    
    textView.font = [UIFont avenirRegularWithSize:16.0f];
    textView.textColor = [UIColor furbBlue];
    textView.placeholder = str;
    textView.placeholderTextColor = [UIColor furbBlue];
    textView.floatingLabelActiveTextColor = [UIColor furbYellow];
    textView.floatingLabelFont = [UIFont avenirBoldWithSize:11.0f];
    textView.floatingLabelTextColor = [UIColor furbBlue];
    
    [textView setBackgroundColor:[UIColor clearColor]];
}

- (void)createNavigationBar {
    UINavigationBar *navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    [navigationBar setBarTintColor:[UIColor furbBlue]];
    [navigationBar setTintColor:[UIColor whiteColor]];
    [navigationBar setTranslucent:NO];
    [navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                            NSFontAttributeName: [UIFont avenirDemiWithSize:18.0f]}];
    [self.view addSubview:navigationBar];
    
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@"Cadastro"];
    
    UIImage *backImg = [[UIImage imageNamed:@"ic_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [navItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:backImg
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(touchDismiss)]];
    [navigationBar setItems:@[navItem]];
    [[self findHairlineImageViewUnder:navigationBar] setHidden:YES];
}

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

- (void)touchDismiss {
    [self.navigationController popViewControllerAnimated:YES];
}


-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField != _dtNascimentoField) {
        return YES;
    }
    VMaskTextField * maskTextField = (VMaskTextField*) textField;
    return  [maskTextField shouldChangeCharactersInRange:range replacementString:string];

}

@end
