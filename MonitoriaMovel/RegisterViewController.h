//
//  RegisterViewController.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 09/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface RegisterViewController : UIViewController
- (instancetype)initWithUser:(User *) user;
@end
