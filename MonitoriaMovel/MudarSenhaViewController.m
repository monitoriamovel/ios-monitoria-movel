//
//  MudarSenhaViewController.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 19/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "MudarSenhaViewController.h"
#import "UIFont+MM.h"
#import "UIColor+MM.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"
#import "SVProgressHUD.h"
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "CryptCommons.h"

@interface MudarSenhaViewController ()

@property (nonatomic, strong) JVFloatLabeledTextField *senhaField;
@property (nonatomic, strong) JVFloatLabeledTextField *confirmSenhaField;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton *forgotButton;

@end

@implementation MudarSenhaViewController


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.view setBackgroundColor:[UIColor colorFromHexString:@"f2f2f2"]];
    
    [self configureNavigationBar];
    
    [self createUI];
    
}

- (void) createUI {
    
    UIImage *imgBack = [[UIImage imageNamed:@"ic_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:imgBack
                                                                               style:UIBarButtonItemStylePlain
                                                                              target:self action:@selector(touchDismiss)]];
    

    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:_scrollView];
    
    
    float width = _scrollView.frame.size.width;
    float height = _scrollView.frame.size.height;
    
    
    int y = 10;
    
    
    _senhaField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [self configureTextField:_senhaField withString:@"Nova senha"];
    [_senhaField addTarget:_senhaField action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [_scrollView addSubview:_senhaField];
    _senhaField.keepBaseline = YES;
    
    y += 60;
    
    
    _confirmSenhaField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [self configureTextField:_confirmSenhaField withString:@"Repita a nova senha"];
    [_confirmSenhaField addTarget:_confirmSenhaField action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [_scrollView addSubview:_confirmSenhaField];
    _confirmSenhaField.keepBaseline = YES;
    
    y += 70;
    
    _forgotButton = [[UIButton alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [_forgotButton addTarget:self action:@selector(touchNewPassword) forControlEvents:UIControlEventTouchUpInside];
    [_forgotButton setTitle:@"Alterar senha" forState:UIControlStateNormal];
    [_forgotButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_forgotButton setBackgroundColor:[UIColor furbBlue]];
    [_forgotButton.layer setCornerRadius:2.0f];
    [_scrollView addSubview:_forgotButton];
    
    
    [_scrollView setContentSize:CGSizeMake(width, y+60)];
    
}


- (void) touchNewPassword {
    
    [self.view endEditing:YES];

    
    if([_senhaField.text length] == 0 || (![_senhaField.text isEqualToString:_confirmSenhaField.text])) {
        [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Preencha corretamente as senhas!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Alterando..."];
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"usuario"];
    
    [query getObjectInBackgroundWithId:[AppDelegate sharedDelegate].loggedUser.myself.objectId block:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if(!error) {
            object[@"senha"] = [CryptCommons md5:_senhaField.text];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                [SVProgressHUD dismiss];

                if (succeeded) {
                    [_senhaField setText:@""];
                    [_confirmSenhaField setText:@""];
                    
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                    [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Senha alterada com sucesso." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                } else {
                    [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Não foi possível alterar sua senha.\nTente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                    
                }
                

            }];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Não foi possível alterar sua senha.\nTente mais tarde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }];
    
    
    
    
}

- (void) configureTextField:(JVFloatLabeledTextField *)textField withString:(NSString *) str{
    
    //Field settings
    textField.font = [UIFont avenirRegularWithSize:16.0f];
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str
                                                                      attributes:@{NSForegroundColorAttributeName: [UIColor furbBlue]}];
    textField.floatingLabelFont = [UIFont avenirBoldWithSize:11.0f];
    textField.floatingLabelTextColor = [UIColor furbBlue];
    textField.floatingLabelActiveTextColor = [UIColor furbYellow];
    [textField setTextColor:[UIColor furbBlue]];
    [textField setSecureTextEntry:YES];
    
    //Keyboard settings
    [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [textField setReturnKeyType:UIReturnKeyDone];
}


- (void)configureNavigationBar {
    self.title = @"Alteração de Senha";
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar setBarTintColor:[UIColor furbBlue]];
    [navigationBar setTintColor:[UIColor whiteColor]];
    [navigationBar setTranslucent:NO];
    [navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                            NSFontAttributeName: [UIFont avenirDemiWithSize:18.0f]}];
    
    [[self findHairlineImageViewUnder:navigationBar] setHidden:YES];

}

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

- (void)touchDismiss {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
