//
//  ChatTableViewCell.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "ChatTableViewCell.h"
#import "UIColor+MM.h"
#import "UIFont+MM.h"
#import "ImageProvider.h"


@interface ChatTableViewCell ()
@property (nonatomic, strong) UIImageView *photo;
@property (nonatomic, strong) UILabel *title;
@end

@implementation ChatTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        
        _photo = [UIImageView new];
        [_photo setClipsToBounds:YES];
        [_photo setContentMode:UIViewContentModeScaleAspectFill];
        [self.contentView addSubview:_photo];
        
        _title = [UILabel new];
        [_title setFont:[UIFont avenirMediumWithSize:16.0f]];
        [_title setTextColor:[UIColor darkGrayColor]];
        [_title setNumberOfLines:2];
        [self.contentView addSubview:_title];
        
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return self;
}

- (void) setUser:(SBDUser *) usr andLastMessage:(NSString *) lastMessage {
    [[ImageProvider defaultProvider] imageWithURL:[NSURL URLWithString:usr.profileUrl] completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            [_photo setImage:image];
        } else {
            [_photo setImage:[UIImage imageNamed:@"user_no_photo"]];
        }
    }];
    
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:usr.nickname
                                                                             attributes:@{NSFontAttributeName:[UIFont avenirMediumWithSize:16.0f],
                                                                                          NSForegroundColorAttributeName : [UIColor darkGrayColor]}];
    
    
    if(lastMessage) {
        
        [text appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", lastMessage]
                                                                            attributes:@{NSFontAttributeName:[UIFont avenirMediumWithSize:13.0f],
                                                                                         NSForegroundColorAttributeName : [UIColor lightGrayColor]}]];
        
    }
    [self.title setAttributedText:text];

    
}

-(void)layoutSubviews {
    [super layoutSubviews];
    float width = self.frame.size.width;
    float height = self.frame.size.height;
    
    float imgSize = height-20;
    
    [_photo setFrame:CGRectMake(10, (height-imgSize)/2, imgSize, imgSize)];
    [_photo.layer setCornerRadius:imgSize/2.0f];
    
    [_title setFrame:CGRectMake(_photo.frame.origin.x+_photo.frame.size.width+10, 10, width-_photo.frame.origin.x+_photo.frame.size.width+20, height-20)];
    
}
@end
