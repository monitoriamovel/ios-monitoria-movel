//
//  AtendimentoTableViewCell.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 19/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "AtendimentoTableViewCell.h"
#import "DateFormatter.h"
#import "UIFont+MM.h"
#import "UIColor+MM.h"
#import "AppDelegate.h"

@interface AtendimentoTableViewCell()
@property (nonatomic, strong) UIImageView *photo;
@property (nonatomic, strong) UILabel *title;
@end

@implementation AtendimentoTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        
        
        _photo = [UIImageView new];
        [_photo setClipsToBounds:YES];
        [_photo setContentMode:UIViewContentModeScaleAspectFill];
        [self.contentView addSubview:_photo];
        
        _title = [UILabel new];
        [_title setFont:[UIFont avenirMediumWithSize:16.0f]];
        [_title setTextColor:[UIColor darkGrayColor]];
        [_title setNumberOfLines:2];
        [self.contentView addSubview:_title];
        
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

- (void) setAtendimento:(Atendimento *) atendimento{
    
    if([atendimento.usuario.guid isEqualToString:[AppDelegate sharedDelegate].loggedUser.guid]) {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    } else {
        [self.contentView setBackgroundColor:[UIColor colorFromHexString:@"FFFFCC"]];
    }
    
    [atendimento.usuario getFotoWithcompletionBlock:^(UIImage *image) {
        [_photo setImage:image];
    }];
    
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:atendimento.usuario.nome
                                                                             attributes:@{NSFontAttributeName:[UIFont avenirMediumWithSize:16.0f],
                                                                                          NSForegroundColorAttributeName : [UIColor darkGrayColor]}];
    
    NSString *horarioMarcadoFormatado = [[DateFormatter sharedFormatter] diaHoraFormatter:atendimento.horarioMarcado];
    
    
    
    [text appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", horarioMarcadoFormatado]
                                                                        attributes:@{NSFontAttributeName:[UIFont avenirMediumWithSize:13.0f],
                                                                                     NSForegroundColorAttributeName : [UIColor lightGrayColor]}]];
    [self.title setAttributedText:text];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    float width = self.frame.size.width;
    float height = self.frame.size.height;
    
    float imgSize = height-20;
    
    [_photo setFrame:CGRectMake(10, (height-imgSize)/2, imgSize, imgSize)];
    [_photo.layer setCornerRadius:imgSize/2.0f];
    
    [_title setFrame:CGRectMake(_photo.frame.origin.x+_photo.frame.size.width+10, 20, width-_photo.frame.origin.x+_photo.frame.size.width+20, height-40)];
}


@end
