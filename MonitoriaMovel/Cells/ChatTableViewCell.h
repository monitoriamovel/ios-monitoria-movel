//
//  ChatTableViewCell.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import <SendBirdSDK/SBDUser.h>

@interface ChatTableViewCell : UITableViewCell
- (void) setUser:(SBDUser *) usr andLastMessage:(NSString *) lastMessage;

@end
