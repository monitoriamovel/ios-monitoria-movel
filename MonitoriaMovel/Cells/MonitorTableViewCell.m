//
//  MonitorTableViewCell.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 09/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "MonitorTableViewCell.h"
#import "UIFont+MM.h"
#import "UIColor+MM.h"

@interface MonitorTableViewCell()
@property (nonatomic, strong) UIImageView *photo;
@property (nonatomic, strong) UILabel *title;

@end

@implementation MonitorTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        
        _photo = [UIImageView new];
        [_photo setClipsToBounds:YES];
        [_photo setContentMode:UIViewContentModeScaleAspectFill];
        [self.contentView addSubview:_photo];
        
        _title = [UILabel new];
        [_title setFont:[UIFont avenirMediumWithSize:16.0f]];
        [_title setTextColor:[UIColor darkGrayColor]];
        [_title setNumberOfLines:2];
        [self.contentView addSubview:_title];
        
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return self;
}

- (void) setUser:(User *) usr{
    [usr getFotoWithcompletionBlock:^(UIImage *image) {
        [_photo setImage:image];
    }];
    
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:usr.nome
                                                                             attributes:@{NSFontAttributeName:[UIFont avenirMediumWithSize:16.0f],
                                                                                          NSForegroundColorAttributeName : [UIColor darkGrayColor]}];
    
    NSString *especialidadesStr = usr.especialidades;
    
//    NSString *especialidadesStr = @"Java, C++, iOS";
//    int qtdeMaximaIteracoes = [usr.especialidades count] > 10 ? 10 : (int)[usr.especialidades count];
//   
//    for (int i = 0; i < qtdeMaximaIteracoes; i++) {
//        NSString *stringFormat = @", %@";
//        if(i == 0) {
//            stringFormat = @"%@";
//        }
//        
//        especialidadesStr = [especialidadesStr stringByAppendingString:[NSString stringWithFormat:stringFormat, [usr.especialidades[i] descricao]]];
//
//    }
    
    
    [text appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", especialidadesStr]
                                                                        attributes:@{NSFontAttributeName:[UIFont avenirMediumWithSize:13.0f],
                                                                                     NSForegroundColorAttributeName : [UIColor lightGrayColor]}]];
    [self.title setAttributedText:text];
    
}

-(void)layoutSubviews {
    [super layoutSubviews];
    float width = self.frame.size.width;
    float height = self.frame.size.height;
    
    float imgSize = height-20;
    
    [_photo setFrame:CGRectMake(10, (height-imgSize)/2, imgSize, imgSize)];
    [_photo.layer setCornerRadius:imgSize/2.0f];
    
    [_title setFrame:CGRectMake(_photo.frame.origin.x+_photo.frame.size.width+10, 20, width-_photo.frame.origin.x+_photo.frame.size.width+20, height-40)];
    
}


@end
