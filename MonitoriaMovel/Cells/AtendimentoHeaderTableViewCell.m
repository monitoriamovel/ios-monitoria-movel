//
//  AtendimentoHeaderTableViewCell.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 19/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "AtendimentoHeaderTableViewCell.h"
#import "UIFont+MM.h"
#import "UIColor+MM.h"
#import "DateFormatter.h"

@interface AtendimentoHeaderTableViewCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@end

@implementation AtendimentoHeaderTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        
        [self.contentView setBackgroundColor:[UIColor colorFromHexString:@"f2f2f2"]];
        
        _titleLabel = [UILabel new];
        [_titleLabel setFont:[UIFont avenirBoldWithSize:16.0f]];
        [_titleLabel setTextColor:[UIColor darkGrayColor]];
        [self.contentView addSubview:_titleLabel];
        
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return self;
}

- (void) setDate:(NSDate *) date{
    [_titleLabel setText:[[DateFormatter sharedFormatter] diaFormatter:date]];
}

- (void) setTipoAguardandoAgendamento:(int) cod{
    // Atendimento:
    // 0 = pendente pro monitor verificar se pode ou n
    // 1 = rejeitado -> ver um novo horário
    if(cod == 0) {
        [_titleLabel setText:@"Pendentes"];
    } else if (cod == 1) {
        [_titleLabel setText:@"Rejeitados"];
    }
}

-(void)layoutSubviews {
    [super layoutSubviews];
    float width = self.frame.size.width;
    float height = self.frame.size.height;
    
    [_titleLabel setFrame:CGRectMake(10, 5, width-20, height-10)];
    
}

@end
