//
//  AtendimentoHeaderTableViewCell.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 19/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AtendimentoHeaderTableViewCell : UITableViewCell
- (void) setDate:(NSDate *) date;
- (void) setTipoAguardandoAgendamento:(int) cod;
@end
