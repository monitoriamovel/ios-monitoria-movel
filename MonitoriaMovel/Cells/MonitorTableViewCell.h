//
//  MonitorTableViewCell.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 09/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface MonitorTableViewCell : UITableViewCell
- (void) setUser:(User *) usr;
@end
