//
//  CalendarViewController.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "CalendarViewController.h"
#import "UIColor+MM.h"
#import "UIFont+MM.h"
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "Atendimento.h"
#import "AtendimentosPendentesViewController.h"
#import "DateFormatter.h"
#import "AtendimentoHeaderTableViewCell.h"
#import "AtendimentoTableViewCell.h"
#import "AvaliaAtendimentoViewController.h"

@interface CalendarViewController () <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableDictionary<NSDate *, NSMutableArray<Atendimento *> *> *atendimentos;
@property (nonatomic, strong) NSMutableArray<NSDate *> *datesArray;

@property (nonatomic, assign) int selectedIndex;
@property (nonatomic, assign) int selectedSection;

@end

@implementation CalendarViewController

static NSString *kCompromissoCell = @"kCompromissoCell";
static NSString *kHeaderCell = @"kHeaderCell";

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        self.dateFormatter.dateFormat = @"dd/MM/yyyy";
        _datesArray = [NSMutableArray array];
        _atendimentos = [NSMutableDictionary dictionary];

    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureNavigationBar];
    [self loadData];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    float width = self.view.frame.size.width;
    float height = self.view.frame.size.height;
    
    float topMargin = 0;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, topMargin, width, height-64-44-topMargin) style:UITableViewStylePlain];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [_tableView registerClass:[AtendimentoTableViewCell class] forCellReuseIdentifier:kCompromissoCell];
    [_tableView registerClass:[AtendimentoHeaderTableViewCell class] forCellReuseIdentifier:kHeaderCell];
    [self.view addSubview:_tableView];
    
}


- (void) loadData {
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"usuario = %@ OR monitor = %@",
                              [[AppDelegate sharedDelegate].loggedUser getParseObject],
                              [[AppDelegate sharedDelegate].loggedUser getParseObject]
                              ];
    

    PFQuery *query = [PFQuery queryWithClassName:@"atendimento" predicate:predicate];
    
//    [query whereKey:@"usuario" equalTo:[[AppDelegate sharedDelegate].loggedUser getParseObject]];
    [query whereKey:@"status" equalTo:@(1)];
    [query whereKey:@"encerrado" notEqualTo:@(1)];
    
    //do dia atual pro mais novo
    [query orderByAscending:@"horario_marcado"];

    [query includeKeys:@[@"monitor", @"usuario"]];
    
	
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if(!error){
            [_atendimentos removeAllObjects];
            [_datesArray removeAllObjects];
        }
        
        if (!error && [objects count] > 0) {
            
            for (PFObject *obj in objects) {
                Atendimento *at = [[Atendimento alloc] initWithParseObject:obj];
                
                NSDate *formattedDate = [[DateFormatter sharedFormatter] dateByIgnoringTimeComponentsOfDate:at.horarioMarcado];
                
                if(![_atendimentos objectForKey:formattedDate]) {
                    [_datesArray addObject:formattedDate];
                }
                
                if(!_atendimentos[formattedDate]) {
                    _atendimentos[formattedDate] = [NSMutableArray array];
                }
                [[_atendimentos objectForKey:formattedDate] addObject:at];
            }
            
        }
        [_tableView reloadData];

    }];

}

#pragma mark - UIAlerview Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        //Não faz nada
    } else if (buttonIndex == 1) {
        Atendimento *at = [_atendimentos objectForKey:_datesArray[_selectedSection]][_selectedIndex-1];
        [self.navigationController pushViewController:[[AvaliaAtendimentoViewController alloc] initWithAtendimento:at] animated:YES];
    }
}

#pragma mark - UITableView Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row > 0) {
        _selectedIndex = (int) indexPath.row;
        _selectedSection = (int) indexPath.section;
        
        Atendimento *at = [[_atendimentos objectForKey:_datesArray[_selectedSection]] objectAtIndex:_selectedIndex-1];
        
        //Somente o usuario pode encerrar o atendimetno
        if([at.usuario.guid isEqualToString:[AppDelegate sharedDelegate].loggedUser.guid]) {
            [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Você deseja encerrar esse atendimento?" delegate:self cancelButtonTitle:@"Não" otherButtonTitles:@"Sim", nil] show];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Por favor, solicite ao usuário que encerre o atendimento" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_datesArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ [_atendimentos objectForKey:_datesArray[section]] count]+1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0) {
        
        AtendimentoHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kHeaderCell forIndexPath:indexPath];
        
        if(!cell) {
            cell = [[AtendimentoHeaderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kHeaderCell];
        }
        [(AtendimentoHeaderTableViewCell *)cell setDate:_datesArray[indexPath.section]];
        [cell layoutSubviews];
    
        return cell;
        
    } else {
        
       AtendimentoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCompromissoCell forIndexPath:indexPath];
        
        if(!cell) {
            cell = [[AtendimentoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCompromissoCell];
        }
        
        [cell layoutSubviews];
        
        Atendimento *at = [[_atendimentos objectForKey:_datesArray[indexPath.section]] objectAtIndex:indexPath.row-1];
        [cell setAtendimento:at];
        

        return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0) {
        return 30.0;
    } else {
        return 95.0f;
    }
}

#pragma mark - Tabbar configuration

- (void)configureNavigationBar {
    self.title = @"Agenda";
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar setBarTintColor:[UIColor furbBlue]];
    [navigationBar setTintColor:[UIColor whiteColor]];
    [navigationBar setTranslucent:NO];
    [navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                            NSFontAttributeName: [UIFont avenirDemiWithSize:18.0f]}];
    
    [[self findHairlineImageViewUnder:navigationBar] setHidden:YES];
    
    
    
    UIButton *newAgendamentoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [newAgendamentoButton setImage:[UIImage imageNamed:@"ic_waiting_list"] forState:UIControlStateNormal];
    [newAgendamentoButton addTarget:self action:@selector(touchListaPendentes) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *RightButton=[[UIBarButtonItem alloc] initWithCustomView:newAgendamentoButton];
    self.navigationItem.rightBarButtonItem=RightButton;

}

- (void) touchListaPendentes {
    [self.navigationController pushViewController:[[AtendimentosPendentesViewController alloc] init] animated:YES];
}

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

@end
