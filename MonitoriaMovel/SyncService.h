//
//  SyncService.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 20/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SyncService : NSObject
+ (void) resetSenhaWithEmail:(NSString *) email andCompletionBlock:(void(^)(BOOL success, id data))completionBlock;

@end
