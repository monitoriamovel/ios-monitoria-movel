//
//  SyncService.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 20/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "SyncService.h"

@implementation SyncService

+ (void) resetSenhaWithEmail:(NSString *) email andCompletionBlock:(void(^)(BOOL success, id data))completionBlock {
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"https://monitoriamovelweb.herokuapp.com/recoverMail?email=%@", email]];
    
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        BOOL result = NO;
        id resultObj = nil;
        
        if(!error){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingAllowFragments
                                                                   error:&error];
            result = YES;
            resultObj = json;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            completionBlock(result, resultObj);
        });
    }];
}
@end
