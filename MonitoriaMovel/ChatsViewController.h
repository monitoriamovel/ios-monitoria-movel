//
//  ChatsViewController.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SendBirdSDK/SendBirdSDK.h>
@interface ChatsViewController : UIViewController
- (void) openChatScreenWithChannel:(SBDGroupChannel *) channel andUser:(SBDUser *)user;
@end
