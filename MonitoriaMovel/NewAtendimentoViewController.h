//
//  NewAtendimentoViewController.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 07/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "Atendimento.h"
@interface NewAtendimentoViewController : UIViewController
- (instancetype)initWithMonitorSelecionado:(User *) monitor;
- (instancetype)initWithAtendimentoSelecionado:(Atendimento *) atendimento;
@end
