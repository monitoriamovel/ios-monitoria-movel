//
//  ForgotPasswordViewController.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 09/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "UIFont+MM.h"
#import "UIColor+MM.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"
#import "SVProgressHUD.h"
#import "SyncService.h"

@interface ForgotPasswordViewController ()
@property (nonatomic, strong) JVFloatLabeledTextField *emailField;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton *forgotButton;

@end

@implementation ForgotPasswordViewController



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.view setBackgroundColor:[UIColor colorFromHexString:@"f2f2f2"]];
    
    [self createNavigationBar];
    
    [self createUI];
    
}

- (void) createUI {
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    [self.view addSubview:_scrollView];
    
    
    float width = _scrollView.frame.size.width;
    float height = _scrollView.frame.size.height;
    
    
    int y = 30;
    

    _emailField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [self configureTextField:_emailField withString:@"E-mail"];
    [_emailField addTarget:_emailField action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [_scrollView addSubview:_emailField];
    _emailField.keepBaseline = YES;
    
    y += 70;
    
    _forgotButton = [[UIButton alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [_forgotButton addTarget:self action:@selector(touchNewPassword) forControlEvents:UIControlEventTouchUpInside];
    [_forgotButton setTitle:@"Solicitar nova senha" forState:UIControlStateNormal];
    [_forgotButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_forgotButton setBackgroundColor:[UIColor furbBlue]];
    [_forgotButton.layer setCornerRadius:2.0f];
    [_scrollView addSubview:_forgotButton];
    
    
    [_scrollView setContentSize:CGSizeMake(width, y+60)];
    
}


- (void) touchNewPassword {
    [self.view endEditing:YES];

    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    if([_emailField.text length] == 0 || ![emailTest evaluateWithObject:_emailField.text])
    {
        [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"E-mail inválido!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    
    
    [SVProgressHUD showWithStatus:@"Solicitando..."];
    
    [SyncService resetSenhaWithEmail:_emailField.text andCompletionBlock:^(BOOL success, id data) {
        if (success) {
            [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Um e-mail foi enviado para seu endereço de e-mail" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            [_emailField setText:@""];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"E-mail não encontrado em nossa base de dados." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            
        }
        [SVProgressHUD dismiss];
    }];
    
    


    
}

- (void) configureTextField:(JVFloatLabeledTextField *)textField withString:(NSString *) str{
    
    //Field settings
    textField.font = [UIFont avenirRegularWithSize:16.0f];
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str
                                                                      attributes:@{NSForegroundColorAttributeName: [UIColor furbBlue]}];
    textField.floatingLabelFont = [UIFont avenirBoldWithSize:11.0f];
    textField.floatingLabelTextColor = [UIColor furbBlue];
    textField.floatingLabelActiveTextColor = [UIColor furbYellow];
    [textField setTextColor:[UIColor furbBlue]];
    
    
    //Keyboard settings
    [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [textField setReturnKeyType:UIReturnKeyDone];
}


- (void)createNavigationBar {
    UINavigationBar *navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    [navigationBar setBarTintColor:[UIColor furbBlue]];
    [navigationBar setTintColor:[UIColor whiteColor]];
    [navigationBar setTranslucent:NO];
    [navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                            NSFontAttributeName: [UIFont avenirDemiWithSize:18.0f]}];
    [self.view addSubview:navigationBar];
    
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@"Esqueci a senha"];
    
    UIImage *backImg = [[UIImage imageNamed:@"ic_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [navItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:backImg
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(touchDismiss)]];
    [navigationBar setItems:@[navItem]];
    [[self findHairlineImageViewUnder:navigationBar] setHidden:YES];
}

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

- (void)touchDismiss {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
