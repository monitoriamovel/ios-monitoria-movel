//
//  AtendimentosPendentesViewController.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 07/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "AtendimentosPendentesViewController.h"
#import "Atendimento.h"
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "AtendimentoTableViewCell.h"
#import "UIColor+MM.h"
#import "AtendimentoHeaderTableViewCell.h"
#import "NewAtendimentoViewController.h"

@interface AtendimentosPendentesViewController () <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *atendimentosPendentes;
@property (nonatomic, strong) NSMutableArray *atendimentosRejeitados;
@property (nonatomic, assign) int selectedIndex;

@end

@implementation AtendimentosPendentesViewController

static NSString *kAtendimentoCell = @"kAtendimentoCell";
static NSString *kAtendimentoHeaderCell = @"kAtendimentoHeaderCell";

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"Agendamentos pendentes";
        _atendimentosPendentes = [NSMutableArray array];
        _atendimentosRejeitados = [NSMutableArray array];
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *imgBack = [[UIImage imageNamed:@"ic_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:imgBack
                                                                               style:UIBarButtonItemStylePlain
                                                                              target:self action:@selector(dismiss)]];
    
    
    [self.view setBackgroundColor:[UIColor colorFromHexString:@"f2f2f2"]];

    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [_tableView registerClass:[AtendimentoTableViewCell class] forCellReuseIdentifier:kAtendimentoCell];
    [_tableView registerClass:[AtendimentoHeaderTableViewCell class] forCellReuseIdentifier:kAtendimentoHeaderCell];
    [self.view addSubview:_tableView];
    
    [self loadData];
}


- (void) loadData {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"usuario = %@ OR monitor = %@",
                              [[AppDelegate sharedDelegate].loggedUser getParseObject],
                              [[AppDelegate sharedDelegate].loggedUser getParseObject]
                              ];

    PFQuery *query = [PFQuery queryWithClassName:@"atendimento" predicate:predicate];
    
    [query whereKey:@"status" notEqualTo:@(1)];
    [query whereKey:@"encerrado" notEqualTo:@(1)];
    [query orderByAscending:@"status"];

    [query includeKeys:@[@"monitor", @"usuario"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error && [objects count] > 0) {
            [_atendimentosPendentes removeAllObjects];
            [_atendimentosRejeitados removeAllObjects];
            
            for (PFObject *obj in objects) {
                
                Atendimento *at = [[Atendimento alloc] initWithParseObject:obj];
                
                if(at.status == 0) {
                    [_atendimentosPendentes addObject:at];
                } else {
                    [_atendimentosRejeitados addObject:at];
                }
                
            }
            
            if(_tableView){
                [_tableView reloadData];
            }
        }
        
        
    }];
    
}


- (void)dismiss {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0) {
        return 30.0f;
    }
    return 95.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return [_atendimentosPendentes count] + 1;
    }
    return [_atendimentosRejeitados count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row > 0) {
        AtendimentoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAtendimentoCell forIndexPath:indexPath];
        
        if(!cell) {
            cell = [[AtendimentoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kAtendimentoCell];
        }
        [cell layoutSubviews];

        if(indexPath.section == 0) {
            [cell setAtendimento:_atendimentosPendentes[indexPath.row-1]];
        } else {
            [cell setAtendimento:_atendimentosRejeitados[indexPath.row-1]];
        }
        
        
        return cell;
    } else {
        AtendimentoHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAtendimentoHeaderCell forIndexPath:indexPath];
        
        if(!cell) {
            cell = [[AtendimentoHeaderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kAtendimentoHeaderCell];
        }
        [(AtendimentoHeaderTableViewCell *)cell setTipoAguardandoAgendamento:(int)indexPath.section];
        [cell layoutSubviews];
        
        return cell;
        
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return;
    }
    _selectedIndex = (int) indexPath.row;
    Atendimento * at;
    
   
    if(indexPath.section == 0) {
        at = _atendimentosPendentes[_selectedIndex-1];
        
        
        if([at.usuario.guid isEqualToString:[AppDelegate sharedDelegate].loggedUser.guid]) {
            [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Aguarde o monitor validar o horário." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            return;
        }
        
    } else {
        at = _atendimentosRejeitados[_selectedIndex-1];
    }
    

    if(at.status == 0) {
        [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Você aceita esse horário?" delegate:self cancelButtonTitle:@"Ver depois" otherButtonTitles:@"Sim", @"Não", nil] show];
    } else {
        if([at.monitor.guid isEqualToString:[AppDelegate sharedDelegate].loggedUser.guid]) {
            [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Aguarde o usuário escolher um novo horário." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        } else {
            [self.navigationController pushViewController:[[NewAtendimentoViewController alloc] initWithAtendimentoSelecionado:at] animated:YES];
        }
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"Button index; %li", (long)buttonIndex);
    if(buttonIndex == 0) {
        //Ver depois
    } else if(buttonIndex == 1) {
        //Sim
        
        PFQuery *query = [PFQuery queryWithClassName:@"atendimento"];
        Atendimento *at = _atendimentosPendentes[_selectedIndex-1];
        
        [query getObjectInBackgroundWithId:at.myself.objectId block:^(PFObject * _Nullable object, NSError * _Nullable error) {
            if(!error) {
                object[@"status"] = @(1);
                [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    [self loadData];
                }];
            }

        }];
        
    } else if (buttonIndex == 2) {
        //Não
        PFQuery *query = [PFQuery queryWithClassName:@"atendimento"];
        Atendimento *at = _atendimentosPendentes[_selectedIndex-1];
        
        [query getObjectInBackgroundWithId:at.myself.objectId block:^(PFObject * _Nullable object, NSError * _Nullable error) {
            if(!error) {
                object[@"status"] = @(2);
                [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    [self loadData];
                }];
            }
        }];
    }
}

@end
