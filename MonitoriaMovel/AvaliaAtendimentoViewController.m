//
//  NewAtendimentoViewController.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 07/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "AvaliaAtendimentoViewController.h"
#import "JVFloatLabeledTextField.h"
#import "UIColor+MM.h"
#import "UIFont+MM.h"
#import "DateFormatter.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>

@interface AvaliaAtendimentoViewController ()<UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIPickerView *notaPicker;

@property (nonatomic, strong) JVFloatLabeledTextField *notaField;

@property (nonatomic, strong) UIButton *agendarButton;
@property (nonatomic, strong) Atendimento *atendimentoSelecionado;

@end

@implementation AvaliaAtendimentoViewController

- (instancetype)initWithAtendimento:(Atendimento *) atendimento
{
    self = [super init];
    if (self) {
        _atendimentoSelecionado = atendimento;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Avaliação de atendimento";
    
    UIImage *imgBack = [[UIImage imageNamed:@"ic_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:imgBack
                                                                               style:UIBarButtonItemStylePlain
                                                                              target:self action:@selector(dismiss)]];
    
    
    [self.view setBackgroundColor:[UIColor colorFromHexString:@"f2f2f2"]];
    
    
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:_scrollView];
    
    float width = _scrollView.frame.size.width;
    
    int y = 20;
    
    _notaPicker = [UIPickerView new];
    [_notaPicker setDelegate:self];
    [_notaPicker setDataSource:self];
    
    _notaField = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [self configureTextField:_notaField withString:@"Nota"];
    [_notaField addTarget:_notaField action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [_scrollView addSubview:_notaField];
    _notaField.keepBaseline = YES;
    _notaField.inputView = _notaPicker;
    
    
    y += 60;
    
    
    _agendarButton = [[UIButton alloc] initWithFrame:CGRectMake(30, y, width-60, 40)];
    [_agendarButton addTarget:self action:@selector(touchAvaliar) forControlEvents:UIControlEventTouchUpInside];
    [_agendarButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_agendarButton setBackgroundColor:[UIColor furbBlue]];
    [_agendarButton.layer setCornerRadius:2.0f];
    [_agendarButton setTitle:@"Avaliar" forState:UIControlStateNormal];
    [_scrollView addSubview:_agendarButton];
    
    
    [_scrollView setContentSize:CGSizeMake(width, y+60)];
    
    
    
}

#pragma mark - Picker config
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [_notaField setText:[NSString stringWithFormat:@"%li", row+1]];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 10;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    // de 1 a 10
    return [NSString stringWithFormat:@"%li", row+1];
}


#pragma mark - Button methods

- (void) touchAvaliar {
    //Cria no parse uma solicitação
    
    [self.view endEditing:YES];
    
    
    if(_notaField.text.length == 0) {
        [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Por favor, selecione uma nota." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        
        return;
    }
    [SVProgressHUD showWithStatus:@"Avaliando..."];
    
 
    PFQuery *query = [PFQuery queryWithClassName:@"atendimento"];
    [query includeKeys:@[@"monitor", @"usuario"]];
    [query getObjectInBackgroundWithId:_atendimentoSelecionado.myself.objectId block:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if(!error) {
            
            //pega o usuario pra alterar a avlaiacao dele
            
            
            PFQuery *query = [PFQuery queryWithClassName:@"usuario"];
            User *usr = [[User alloc] initWithParseObject:object[@"monitor"]];
            
            [query getObjectInBackgroundWithId:usr.myself.objectId block:^(PFObject * _Nullable object1, NSError * _Nullable error) {
                [SVProgressHUD dismiss];
                if(!error) {
                    if([object1[@"avaliacao"] intValue] == 0) {
                        object1[@"avaliacao"] = @([_notaField.text intValue]);
                    } else {
                        object1[@"avaliacao"] =@( ([object1[@"avaliacao"] doubleValue] + [_notaField.text doubleValue])/2.0f );
                    }
                    [object1 saveInBackground];
                }
            }];

            
            
            
            object[@"encerrado"] = @(1);
            object[@"avaliacao"] = @([_notaField.text intValue]);
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                if(!error) {
                    [SVProgressHUD showSuccessWithStatus:@"Atendimento encerrado e avaliado com sucesso."];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                } else {
                    [SVProgressHUD dismiss];
                     [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Ocorreu um erro ao avaliar.\nTente novamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                }
            }];
        } else {
            [SVProgressHUD dismiss];
            [[[UIAlertView alloc] initWithTitle:@"Monitoria Móvel" message:@"Ocorreu um erro ao avaliar.\nTente novamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }];
    
}

- (void)dismiss {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIView refactor methods

- (void) configureTextField:(JVFloatLabeledTextField *)textField withString:(NSString *) str{
    
    //Field settings
    textField.font = [UIFont avenirRegularWithSize:16.0f];
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str
                                                                      attributes:@{NSForegroundColorAttributeName: [UIColor furbBlue]}];
    textField.floatingLabelFont = [UIFont avenirBoldWithSize:11.0f];
    textField.floatingLabelTextColor = [UIColor furbBlue];
    textField.floatingLabelActiveTextColor = [UIColor furbYellow];
    [textField setTextColor:[UIColor furbBlue]];
    
    
    //Keyboard settings
    [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [textField setReturnKeyType:UIReturnKeyDone];
}

@end
