//
//  ChatsViewController.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "ChatsViewController.h"
#import "UIColor+MM.h"
#import "UIFont+MM.h"
#import "ChatTableViewCell.h"
#import "User.h"
//#import "MessageConversationViewController.h"
#import "AppDelegate.h"
#import "AgoraViewController.h"

@interface ChatsViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray<SBDGroupChannel *> *conversasArray;
@property (nonatomic, strong) User *me;
@end

@implementation ChatsViewController

static NSString *kChatCell = @"kChatCell";


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _me = [AppDelegate sharedDelegate].loggedUser;
        
        //Configure User ID of chat service
        [SBDMain connectWithUserId:_me.guid completionHandler:^(SBDUser * _Nullable user, SBDError * _Nullable error) {
            if(!error) {
                [_me getFotoWithcompletionBlock:^(UIImage *image) {
                    
                    [SBDMain updateCurrentUserInfoWithNickname:_me.nome profileImage:UIImageJPEGRepresentation(image, 1.0f) completionHandler:^(SBDError * _Nullable error) {
                        
                    }];
                }];
                
               
            }
        }];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureNavigationBar];
    [self refreshData];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64-49)];
    [_tableView registerClass:[ChatTableViewCell class] forCellReuseIdentifier:kChatCell];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_tableView];
    
  
    [self refreshData];

}

#pragma mark - UITableView Methods


- (void) refreshData {
    SBDGroupChannelListQuery *query = [SBDGroupChannel createMyGroupChannelListQuery];
    query.includeEmptyChannel = YES; // Include empty group channels.
    [query loadNextPageWithCompletionHandler:^(NSArray<SBDGroupChannel *> * _Nullable channels, SBDError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        _conversasArray = channels;
        [_tableView reloadData];
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_conversasArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kChatCell forIndexPath:indexPath];
    
    if(!cell) {
        cell = [[ChatTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kChatCell];
    }
    [cell layoutSubviews];
    
    
    SBDGroupChannel *conversa = _conversasArray[indexPath.row];
    SBDUser *usuario = [self getUserFromChannelAtIndexPath:indexPath];;

    if(usuario) {
        SBDUserMessage *msg = nil;
        if([conversa.lastMessage isKindOfClass:[SBDUserMessage class]]) {
            msg = (SBDUserMessage *) conversa.lastMessage;
        }
        [cell setUser:usuario andLastMessage:msg.message];
    }
    
    return cell;
}

- (SBDUser *) getUserFromChannelAtIndexPath:(NSIndexPath *) indexPath {
    SBDGroupChannel *conversa = _conversasArray[indexPath.row];
    for (SBDUser *usr in conversa.members) {
        if(![usr.userId isEqualToString:_me.guid]) {
            return usr;
        }
    }
    return nil;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SBDGroupChannel *channel = _conversasArray[indexPath.row];
    SBDUser *usuario = [self getUserFromChannelAtIndexPath:indexPath];;
    
    [self openChatScreenWithChannel:channel andUser:usuario];

}

- (void) openChatScreenWithChannel:(SBDGroupChannel *) channel andUser:(SBDUser *)user{
    
    AgoraViewController *vc = [[AgoraViewController alloc] init];
    [vc setSenderId:[SBDMain getCurrentUser].userId];
    [vc setSenderDisplayName:[SBDMain getCurrentUser].nickname];
    [vc setTitle: user.nickname];
    [vc setChannel: channel];
    
    [self.navigationController pushViewController:vc animated:YES];

}

#pragma mark - Tabbar configuration

- (void)configureNavigationBar {
    self.title = @"Conversas";
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar setBarTintColor:[UIColor furbBlue]];
    [navigationBar setTintColor:[UIColor whiteColor]];
    [navigationBar setTranslucent:NO];
    [navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                            NSFontAttributeName: [UIFont avenirDemiWithSize:18.0f]}];
    
    [[self findHairlineImageViewUnder:navigationBar] setHidden:YES];
}

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}
@end
