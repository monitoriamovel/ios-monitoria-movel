//
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 26/04/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//


#import "UIImage+MM.h"

@implementation UIImage (MonitoriaMovel)
+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

+ (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

- (NSString *)base64WithMaxSize:(CGFloat)maxSize {
    CGFloat width = self.size.width;
    CGFloat height = self.size.height;
    
    if(width > height && width > maxSize) {
        CGFloat factor = maxSize / width;
        width = maxSize;
        height *= factor;
    }
    else if(height > width && height > maxSize) {
        CGFloat factor = maxSize / height;
        width *= factor;
        height = maxSize;
    }
        
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    [self drawInRect:CGRectMake(0, 0, width, height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *pictureData = UIImageJPEGRepresentation(newImage, 1.0f);
    pictureData = [pictureData base64EncodedDataWithOptions:0];
    return [[NSString alloc] initWithData:pictureData encoding:NSUTF8StringEncoding];
}
@end
