//
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 26/04/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MonitoriaMovel)
+ (UIColor *)colorFromHexString:(NSString *)hexStr;
+ (UIColor *)colorFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha;

+ (UIColor *)furbYellow;
+ (UIColor *)furbBlue;
+ (UIColor *)textLightGray;
@end
