//
//  Services.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 13/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "Services.h"

static NSString *wsSaveImg = @"http://www.inf.furb.br/~leschlogl/save_img.php";


@implementation Services

+ (void)saveImage:(NSString *)base64Img
              withCompletionBlock:(void(^)(BOOL success, id data)) completionBlock {
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        BOOL result = NO;
        id resultObj = nil;
        
        NSError *error = nil;
        NSData *requestData = [NSJSONSerialization dataWithJSONObject:@{@"image":base64Img}
                                                              options:0
                                                                error:&error];
        
        if(error)
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                completionBlock(result, @"Falha no sistema");
            });
            return;
        }
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:wsSaveImg]
                                                               cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                           timeoutInterval:60.0f];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:requestData];
        NSURLResponse *response = nil;
        error = nil;
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if(!error)  {
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingAllowFragments error:&error];
            if(!error) {
                NSString *resultStr = [jsonDictionary objectForKey:@"image"];
                if(resultStr) {
                    result = YES;
                    resultObj = resultStr;
                    
                }
            } else {
                result = NO;
                resultObj = error;
            }
        } else {
            result = NO;
            resultObj = error;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            completionBlock(result, resultObj);
        });
    });
    
    
}


@end
