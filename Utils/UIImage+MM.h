//
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 26/04/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface UIImage (MonitoriaMovel)
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (NSString *)encodeToBase64String:(UIImage *)image;
+ (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData;
- (NSString *)base64WithMaxSize:(CGFloat)maxSize;
@end

