//
//  CryptCommons.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CryptCommons : NSObject
+ (NSString *) md5:(NSString *) input;
@end
