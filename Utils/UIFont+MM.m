//
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 26/04/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "UIFont+MM.h"

@implementation UIFont (MonitoriaMovel)

+ (UIFont*)avenirRegularWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"AvenirNext-Regular" size:size];
}
+ (UIFont*)avenirMediumWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"AvenirNext-Medium" size:size];
}

+ (UIFont*)avenirDemiWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"AvenirNext-DemiBold" size:size];
}
+ (UIFont*)avenirBoldWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"AvenirNext-Bold" size:size];
}
+ (UIFont*)avenirObliqueWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Avenir-Oblique" size:size];
}

+ (UIFont*)avenirRomanWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Avenir-Roman" size:size];
}

+ (UIFont*)avenirLightWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Avenir-Light" size:size];
}

+ (UIFont*)avenirBookWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Avenir-Book" size:size];
}

@end
