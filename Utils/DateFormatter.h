//
//  DateFormatter.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateFormatter : NSObject
+ (instancetype)sharedFormatter;

- (NSString *)shortDate:(NSDate *)date;
- (NSString *)diaHoraFormatter:(NSDate *)date;
- (NSString *)diaFormatter:(NSDate *)date;
- (NSDate *)dateFromServer:(NSString *)stringDate;
- (NSString *)dateToServer:(NSDate *)date;
- (NSDate *) dateByIgnoringTimeComponentsOfDate:(NSDate *)date;
@end
