//
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 26/04/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "UIColor+MM.h"

@implementation UIColor (MonitoriaMovel)

+ (unsigned int)intFromHexString:(NSString *)hexStr {
    unsigned int hexInt = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}


+ (UIColor *)colorFromHexString:(NSString *)hexStr {
    return [self colorFromHexString:hexStr alpha:1.0f];
}

+ (UIColor *)colorFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha {
    unsigned int hexint = [UIColor intFromHexString:hexStr];

    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255.0f
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255.0f
                     blue:((CGFloat) (hexint & 0xFF))/255.0f
                    alpha:alpha];
    
    return color;
}

+ (UIColor *) furbYellow {
    return [UIColor colorWithRed:255/255.0f green:205/255.0f blue:0/255.0f alpha:1.0f];
}

+ (UIColor *) furbBlue{
    //Hex: 004B87
    return [UIColor colorWithRed:0/255.0f green:75/255.0f blue:135/255.0f alpha:1.0f];
}

+ (UIColor *)textLightGray {
    return [self colorFromHexString:@"A4ACB7"];
}


@end

