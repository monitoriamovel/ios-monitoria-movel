//
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 26/04/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (MonitoriaMovel)
+ (UIFont*)avenirRegularWithSize:(CGFloat)size;
+ (UIFont*)avenirMediumWithSize:(CGFloat)size;
+ (UIFont*)avenirDemiWithSize:(CGFloat)size;
+ (UIFont*)avenirObliqueWithSize:(CGFloat)size;
+ (UIFont*)avenirRomanWithSize:(CGFloat)size;
+ (UIFont*)avenirBoldWithSize:(CGFloat)size;
+ (UIFont*)avenirLightWithSize:(CGFloat)size;
+ (UIFont*)avenirBookWithSize:(CGFloat)size;
@end
