//
//  DateFormatter.m
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 10/05/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import "DateFormatter.h"
@interface DateFormatter()
@property (nonatomic, strong) NSDateFormatter *defaultFormatter;
@property (nonatomic, strong) NSDateFormatter *serverFormatter;
@property (nonatomic, strong) NSDateFormatter *diaHoraFormatter;
@property (nonatomic, strong) NSDateFormatter *diaFormatter;

@property(nonatomic, strong) NSCalendar *calendar;

@end

@implementation DateFormatter
static long time_day    =    86400;

+ (instancetype)sharedFormatter {
    static dispatch_once_t onceToken;
    static DateFormatter *sharedFormatter;
    
    dispatch_once(&onceToken, ^{
        sharedFormatter = [[DateFormatter alloc] init];
    });
    
    return sharedFormatter;
}

-(instancetype)init {
    if(self = [super init])
    {
        
        self.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];

        self.defaultFormatter = [[NSDateFormatter alloc] init];
        [self.defaultFormatter setDateStyle:NSDateFormatterShortStyle];
        [self.defaultFormatter setTimeStyle:NSDateFormatterNoStyle];
        
        self.diaHoraFormatter = [[NSDateFormatter alloc] init];
        [self.diaHoraFormatter setDateFormat: @"dd/MM/yyyy HH:mm"];

        self.diaFormatter = [[NSDateFormatter alloc] init];
        [self.diaFormatter setDateFormat: @"dd/MM/yyyy"];


        self.serverFormatter = [[NSDateFormatter alloc] init];
//        2017-05-31T13:03:59.183Z - parse date
        [self.serverFormatter setDateFormat: @"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"];
    }
    return self;
}

- (NSString *)dateToServer:(NSDate *)date {
    return [self.serverFormatter stringFromDate:date];
}

- (NSDate *)dateFromServer:(NSString *)stringDate {
    return [self.serverFormatter dateFromString:stringDate];
}

- (NSString *)shortDate:(NSDate *)date {
    return [self.defaultFormatter stringFromDate:date];
}
- (NSString *)diaHoraFormatter:(NSDate *)date {
    return [self.diaHoraFormatter stringFromDate:date];
}

- (NSString *)diaFormatter:(NSDate *)date {
    return [self.diaFormatter stringFromDate:date];
}

- (NSDate *) dateByIgnoringTimeComponentsOfDate:(NSDate *)date {
    NSDateComponents *components = [self.calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour fromDate:date];
    components.hour = 10;
    return [self.calendar dateFromComponents:components];
}

@end
