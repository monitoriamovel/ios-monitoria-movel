//
//  Services.h
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 13/06/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Services : NSObject
+ (void)saveImage:(NSString *)base64Img
withCompletionBlock:(void(^)(BOOL success, id data)) completionBlock;

@end
