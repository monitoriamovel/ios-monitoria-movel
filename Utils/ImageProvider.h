//
//  MonitoriaMovel
//
//  Created by Lucas Eduardo Schlogl on 26/04/17.
//  Copyright © 2017 Lucas Eduardo Schlogl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ImageProvider : NSObject
@property (nonatomic, strong) NSString *providerName;
@property (nonatomic, strong) NSString *customUserAgent;
+ (ImageProvider *)defaultProvider;
- (void)imageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock;
@end
